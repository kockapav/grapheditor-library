﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Layouts.Settings;

namespace GraphEditor
{
    /// <summary>
    /// Unit tests for GraphEditor class
    /// </summary>
    [TestClass]
    public class GraphEditorTest
    {
        private GraphEditor ge;
        private string path;

        public GraphEditorTest()
        {
            ge = new GraphEditor();
            Assert.IsNotNull(ge);
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
        }

        [TestMethod]
        public void GraphEditor_GetVersionString()
        {
            Assert.IsInstanceOfType(ge.GetVersion(), typeof(string));
        }

        [TestMethod]
        public void GraphEditor_GetLayoutsListString()
        {
            string layouts = ge.GetLayoutList();
            Assert.IsNotNull(layouts);
            Assert.IsTrue(layouts.Contains("RandomLayout"));
        }

        [TestMethod]
        public void GraphEditor_GetFormatListString()
        {
            string layouts = ge.GetFormatList();
            Assert.IsNotNull(layouts);
            Assert.IsTrue(layouts.Contains("GEXF"));
        }

        [TestMethod] 
        public void GraphEditor_LoadsGraphs()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf", 0));
            Assert.AreEqual(1, ge.Graphs.Count);
            Assert.IsTrue(ge.Graphs[0].Nodes.Count > 0);
        }

        [TestMethod]
        public void GraphEditor_RecognizesFormat_FromExtension_WhenLoading()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf"));
            Assert.AreEqual(1, ge.Graphs.Count);
            Assert.IsTrue(ge.Graphs[0].Nodes.Count > 0);
        }

        [TestMethod]
        public void GraphEditor_SavesGraphs()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf", 0));
            Assert.IsTrue(ge.SaveGraphToFile(0, path + "\\saved_graph1.gexf"));
        }

        [TestMethod]
        public void GraphEditor_RecognizesFormat_FromExtension_WhenSaving()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf"));
            Assert.IsTrue(ge.SaveGraphToFile(0, path + "\\saved_graph2.gexf"));
        }

        [TestMethod]
        public void GraphEditor_SetsLayout()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf"));
            Assert.IsTrue(ge.SetGraphLayout(0, 0));
            Assert.IsTrue(ge.SaveGraphToFile(0, path + "\\saved_graph3.gexf"));
        }

        [TestMethod]
        public void GraphEditor_SetsLayoutWithSettings()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf"));
            LayoutSettings settings = new LayoutSettings();
            settings.MinX = -500;
            settings.MinY = -500;
            settings.MaxX = 500;
            settings.MaxY = 500;
            Assert.IsTrue(ge.SetGraphLayout(0, 0, settings));
            Assert.IsTrue(ge.SaveGraphToFile(0, path + "\\saved_graph4.gexf"));
        }

        [TestMethod]
        public void GraphEditor_SetsCircleLayoutWithSettings()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf"));
            CircleLayoutSettings settings = new CircleLayoutSettings();
            settings.OriginX = 5000;
            settings.OriginY = 5000;
            settings.NodeRadius = 20;
            Assert.IsTrue(ge.SetGraphLayout(0, 1, settings));
            Assert.IsTrue(ge.SaveGraphToFile(0, path + "\\saved_graph5.gexf"));
        }

        [TestMethod]
        public void GraphEditor_SavesAndLoadsGraphs_InStringFormat()
        {
            Assert.AreEqual(0, ge.LoadGraphFromFile(path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf"));
            string graphString = ge.SaveGraphToString(0, 0);
            Assert.IsNotNull(graphString);
            Assert.AreEqual(1, ge.LoadGraphFromString(graphString, 0));
            Graph gfile = ge.Graphs.Find(graph => graph.Id == 0);
            Graph gstring = ge.Graphs.Find(graph => graph.Id == 1);
            Assert.AreEqual(gfile.Nodes.Count, gstring.Nodes.Count);
            Assert.AreEqual(gfile.Edges.Count, gstring.Edges.Count);
            Assert.AreEqual(gfile.Attributes.Count, gstring.Attributes.Count);
        }
    }
}
