﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Writers;

namespace GraphEditorTest.Core.Writers
{
    /// <summary>
    /// Unit tests for WriterFactory class
    /// </summary>
    [TestClass]
    public class WriterFactoryTest
    {
        [TestMethod]
        public void WriterFactory_ReturnsValidWriter()
        {
            WriterFactory factory = new WriterFactory();
            Assert.IsNotNull(factory);
            IGraphWriter writer = factory.GetWriter(0);
            Assert.IsInstanceOfType(writer, typeof(GEXFWriter));
        }

        [TestMethod]
        public void ParserFactory_ReturnsValidParserFromFileName()
        {
            WriterFactory factory = new WriterFactory();
            Assert.IsNotNull(factory);
            IGraphWriter writer = factory.GetWriterFromPath("random_graph_file.gexf");
            Assert.IsInstanceOfType(writer, typeof(GEXFWriter));
        }
    }
}
