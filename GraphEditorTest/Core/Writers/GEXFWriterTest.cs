﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Writers;


namespace GraphEditorTest.Core.Writers
{
    /// <summary>
    /// Unit tests for GEXFWriter class
    /// </summary>
    [TestClass]
    public class GEXFWriterTest
    {
        private GEXFParser parser;
        private GEXFWriter writer;
        private string path;

        public GEXFWriterTest()
        {
            parser = new GEXFParser();
            writer = new GEXFWriter();
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
        }

        [TestMethod]
        public void GEXFWriter_SavesGraph()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\basic.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph.gexf", g));
        }

        [TestMethod]
        public void GEXFWriter_SavesGraphWithAllAttributes()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf"); 
            Assert.IsNotNull(g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph2.gexf", g));
            Graph g2 = parser.LoadFromFile(2, path + "\\graph2.gexf");
            Assert.IsNotNull(g2);
            Assert.IsTrue(g2.Nodes.Count == g.Nodes.Count);
        }

        [TestMethod]
        public void GEXFWriter_CanSaveCustomXMLGraphFormat()
        {
            CustomXMLParser cp = new CustomXMLParser();
            Graph g = cp.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_customXML_parsed.gexf", g));
        }

        [TestMethod]
        public void GEXFWriter_SavesGraph_VIZPlugin()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\basic-viz.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph-viz.gexf", g));
        }
    }
}
