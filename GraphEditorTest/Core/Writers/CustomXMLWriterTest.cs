﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Writers;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Graphs;


namespace GraphEditorTest.Core.Writers
{
    /// <summary>
    /// Unit tests for CustomXMLWriter class
    /// </summary>
    [TestClass]
    public class CustomXMLWriterTest
    {
        private CustomXMLParser parser;
        private CustomXMLWriter writer;
        private string path;

        public CustomXMLWriterTest()
        {
            parser = new CustomXMLParser();
            writer = new CustomXMLWriter();
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
        }

        [TestMethod]
        public void CustomXMLWriter_SavesGraph()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph.xml", g));
        }

        [TestMethod]
        public void CustomXMLWriter_CanSaveGEXFGraphFormat()
        {
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_GEXF_parsed.xml", g));
        }
    }
}
