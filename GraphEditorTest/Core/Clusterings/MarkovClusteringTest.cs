﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Clusterings;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Clusterings
{
    /// <summary>
    /// Unit tests for MCL (Markov clustering algorithm)
    /// </summary>
    [TestClass]
    public class MarkovClusteringTest
    {
        private string path;
        private CustomXMLParser parser;
        private MarkovClustering mcl;

        public MarkovClusteringTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            mcl = new MarkovClustering();
        }

        [TestMethod]
        public void MarkovClustering_SetsClustering()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsTrue(mcl.ClusterizeGraph(ref g, 2, 4));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_mcl.xml", g));
        }

        [TestMethod]
        public void MarkovClustering_SetsClustering2()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\clustering.xml");
            Assert.IsTrue(mcl.ClusterizeGraph(ref g, 2, 4));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_mcl_clustering.xml", g));
        }

        [TestMethod]
        public void MarkovClustering_SetsClustering3()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\clustering2.xml");
            Assert.IsTrue(mcl.ClusterizeGraph(ref g, 2, 4));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_mcl_clustering2.xml", g));
        }

        [TestMethod]
        public void MarkovClustering_SetsClustering_BigGraph()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsTrue(mcl.ClusterizeGraph(ref g, 2, 4));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_mcl_big.xml", g));
        }
    }
}
