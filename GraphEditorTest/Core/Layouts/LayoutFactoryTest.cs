﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Layouts;
using GraphEditor.Core.Layouts.Settings;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for LayoutFactory class
    /// </summary>
    [TestClass]
    public class LayoutFactoryTest
    {
        [TestMethod]
        public void LayoutFactory_ReturnsValidLayout()
        {
            LayoutFactory factory = new LayoutFactory();
            Assert.IsNotNull(factory);
            IGraphLayout layout = factory.GetLayout(0);
            Assert.IsInstanceOfType(layout, typeof(RandomLayout));
        }

        [TestMethod]
        public void LayoutFactory_ReturnsValidLayoutWithSettings()
        {
            LayoutFactory factory = new LayoutFactory();
            Assert.IsNotNull(factory);
            LayoutSettings settings = new LayoutSettings();
            IGraphLayout layout = factory.GetLayout(0, settings);
            Assert.IsInstanceOfType(layout, typeof(RandomLayout));
        }
    }
}
