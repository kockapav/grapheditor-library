﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using System.Reflection;
using System.IO;
using GraphEditor.Core.Layouts;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for FruchtermanReingoldLayout class.
    /// </summary>
    [TestClass]
    public class ForceAtlasLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private ForceAtlasLayoutSettings settings;
        private ForceAtlasLayout layout;

        public ForceAtlasLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            settings = new ForceAtlasLayoutSettings();
            layout = new ForceAtlasLayout(settings);
        }

        [TestMethod]
        public void ForceAtlasLayout_SetsLayoutAndSavesIntoFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_forceatlas.xml", g));
        }

        [TestMethod]
        public void ForceAtlasLayout_SetsLayoutAndSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_forceatlas.gexf", g));
        }

        [TestMethod]
        public void ForceAtlasLayout_SetsOnBigGraphSavesIntoFile()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            settings.Iterations = 10;
            layout = new ForceAtlasLayout(settings);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_forceatlas_big.gexf", g));
        }

        [TestMethod]
        public void ForceAtlasLayout_SetsOnMidGraphSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_forceatlas_random.gexf", g));
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_forceatlas_mid.gexf", g));
        }
    }
}
