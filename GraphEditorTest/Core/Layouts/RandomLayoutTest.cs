﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Layouts;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Layouts.Settings;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for RandomLayout class.
    /// </summary>
    [TestClass]
    public class RandomLayoutTest
    {
        private GEXFParser parser;
        private LayoutSettings settings;
        private RandomLayout layout;
        private string path;

        public RandomLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new GEXFParser();
            settings = new LayoutSettings();
            settings.MinX = 5000;
            settings.MaxX = 6000;
            settings.MinY = 7000;
            settings.MaxY = 8000;
            layout = new RandomLayout(settings);
        }

        [TestMethod]
        public void RandomLayout_SetsRandomLayoutInRange()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\basic.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            foreach (Node n in g.Nodes)
            {
                Assert.IsTrue(n.X >= 5000 && n.X <= 6000 && n.Y >= 7000 && n.Y <= 8000);
            }
        }

        [TestMethod]
        public void RandomLayout_SetsLayoutAndSavesIntoFile()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph3.gexf", g));
            Graph g2 = parser.LoadFromFile(2, path + "\\graph3.gexf");
            Assert.IsNotNull(g2);
            Assert.IsTrue(g2.Nodes.Count == g.Nodes.Count);
        }

        [TestMethod]
        public void RandomLayout_SetsAttributes_InCustomXMLFormat()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\basic.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_wattr.xml", g));
        }
    }
}
