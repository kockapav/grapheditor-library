﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Writers;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for ScaleLayout class.
    /// </summary>
    [TestClass]
    public class ScaleLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private ScaleLayoutSettings settings;
        private ScaleLayout layout;

        public ScaleLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
        }

        [TestMethod]
        public void ScaleLayout_ExpandsGraph_Saves()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            settings = new ScaleLayoutSettings(g);
            layout = new ScaleLayout(settings);
            settings.Scale = 2;
            Assert.IsNotNull(g);
            Node node = g.Nodes.First();
            double x = node.X;
            double y = node.Y;
            Assert.IsTrue(layout.SetLayout(ref g));
            node = g.Nodes.First();
            Assert.AreEqual(x * 2, node.X, 0.000001);
            Assert.AreEqual(y * 2, node.Y, 0.000001);
            CustomXMLWriter writer = new CustomXMLWriter();
            Assert.IsTrue(writer.SaveToFile(path + "\\basic2_scaledUp.xml", g));
        }

        [TestMethod]
        public void ScaleLayout_ContractsGraph_Saves()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            settings = new ScaleLayoutSettings(g);
            layout = new ScaleLayout(settings);
            settings.Scale = 0.5;
            Assert.IsNotNull(g);
            Node node = g.Nodes.First();
            double x = node.X;
            double y = node.Y;
            Assert.IsTrue(layout.SetLayout(ref g));
            node = g.Nodes.First();
            Assert.AreEqual(x/2, node.X, 0.000001);
            Assert.AreEqual(y/2, node.Y, 0.000001);
            CustomXMLWriter writer = new CustomXMLWriter();
            Assert.IsTrue(writer.SaveToFile(path + "\\basic2_scaledDown.xml", g));
        }
    }
}
