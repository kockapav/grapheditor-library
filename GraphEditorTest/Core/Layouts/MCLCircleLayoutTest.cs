﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Writers;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for MCLCircleLayout class.
    /// </summary>
    [TestClass]
    public class MCLCircleLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private MCLCircleLayoutSettings settings;
        private MCLCircleLayout layout;

        public MCLCircleLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            settings = new MCLCircleLayoutSettings();
            layout = new MCLCircleLayout(settings);
        }

        [TestMethod]
        public void MCLCircleLayout_SetsMCLCircleLayout()
        {
            
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\clustering.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
        }

        [TestMethod]
        public void MCLCircleLayout_SetsMCLCircleLayoutAndSavesIntoFile()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\clustering.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_mclcircle.gexf", g));
        }

        [TestMethod]
        public void MCLCircleLayout_SetsMCLCircleLayoutAndSavesIntoFile2()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\clustering2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_mclcircle2.gexf", g));
        }

        [TestMethod]
        public void MCLCircleLayout_SetsLayoutBigGraph()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsNotNull(g);
            settings = new MCLCircleLayoutSettings();
            settings.InflationsCnt = 50;
            layout = new MCLCircleLayout(settings);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_celegans_mclcircle.gexf", g));
        }
    }
}
