﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using System.Reflection;
using System.IO;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for OpenOrdLayout class.
    /// </summary>
    [TestClass]
    public class OpenOrdLayoutTest
    {
private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private OpenOrdLayoutSettings settings;
        private OpenOrdLayout layout;

        public OpenOrdLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            settings = new OpenOrdLayoutSettings();
            layout = new OpenOrdLayout(settings);
        }

        [TestMethod]
        public void OpenOrdLayout_SetsLayoutAndSavesIntoFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_openord.xml", g));
        }

        [TestMethod]
        public void OpenOrdLayout_SetsLayoutAndSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_openord.gexf", g));
        }

        [TestMethod]
        public void OpenOrdLayout_SetsOnBigGraphSavesIntoFile()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            settings.Iterations = 10;
            layout = new OpenOrdLayout(settings);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_openord_big.gexf", g));
        }

        [TestMethod]
        public void OpenOrdLayout_SetsOnMidGraphSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_openord_random.gexf", g));
            settings.Iterations = 325;
            layout = new OpenOrdLayout(settings);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_openord_mid.gexf", g));
        }
    }
}
