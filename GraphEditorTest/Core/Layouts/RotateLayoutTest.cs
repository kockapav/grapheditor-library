﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Writers;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for RotateLayout class
    /// </summary>
    [TestClass]
    public class RotateLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private RotateLayoutSettings settings;
        private RotateLayout layout;

        public RotateLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
        }

        [TestMethod]
        public void RotateLayout_RotatesGraph_90DegreesClockwise()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            settings = new RotateLayoutSettings(g);
            layout = new RotateLayout(settings);
            Assert.IsNotNull(g);
            Node node = g.Nodes.First();
            double x = node.X;
            double y = node.Y;
            Assert.IsTrue(layout.SetLayout(ref g));
            node = g.Nodes.First();
            Assert.AreEqual(y, node.X, 0.000001);
            Assert.AreEqual(x, node.Y, 0.000001);
        }

        [TestMethod]
        public void RotateLayout_RotatesGraph_90DegreesCounterClockwise()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            settings = new RotateLayoutSettings(g);
            settings.Clockwise = false;
            layout = new RotateLayout(settings);
            Assert.IsNotNull(g);
            Node node = g.Nodes.First();
            double x = node.X;
            double y = node.Y;
            Assert.IsTrue(layout.SetLayout(ref g));
            node = g.Nodes.First();
            Assert.AreEqual(-y, node.X, 0.000001);
            Assert.AreEqual(x, node.Y, 0.000001);
        }

        [TestMethod]
        public void RotateLayout_RotatesGraph_AroundNode()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            settings = new RotateLayoutSettings(g);
            Node node = g.Nodes.First();
            double x = node.X;
            double y = node.Y;
            settings.OriginX = x;
            settings.OriginY = y;
            layout = new RotateLayout(settings);
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            node = g.Nodes.First();
            Assert.AreEqual(x, node.X, 0.000001);
            Assert.AreEqual(y, node.Y, 0.000001);
        }

        [TestMethod]
        public void RotateLayout_RotatesBy45AroundNode_SavesToFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            settings = new RotateLayoutSettings(g);
            settings.Angle = 45;
            settings.OriginX = 0;
            settings.OriginY = 50;
            layout = new RotateLayout(settings);
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\basic2_rotated.xml", g));
        }
    }
}
