﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for YifanHuLayout class.
    /// </summary>
    [TestClass]
    public class YifanHuLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private YifanHuLayoutSettings settings;
        private YifanHuLayout layout;

        public YifanHuLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            settings = new YifanHuLayoutSettings();
            layout = new YifanHuLayout(settings);
        }

        [TestMethod]
        public void YifanHuLayout_SetsLayoutAndSavesIntoFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_yifanhu.xml", g));
        }

        [TestMethod]
        public void YifanHuLayout_SetsLayoutAndSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_yifanhu.gexf", g));
        }

        [TestMethod]
        public void YifanHuLayout_SetsOnBigGraphSavesIntoFile()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            settings.Iterations = 10;
            layout = new YifanHuLayout(settings);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_yifanhu_big.gexf", g));
        }

        [TestMethod]
        public void YifanHuLayout_SetsOnMidGraphSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_yifanhu_random.gexf", g));
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_yifanhu_mid.gexf", g));
        }
    }
}
