﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using GraphEditor.Core.Graphs;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Writers;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for CircleLayout class.
    /// </summary>
    [TestClass]
    public class CircleLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private CircleLayoutSettings settings;
        private CircleLayout layout;

        public CircleLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            settings = new CircleLayoutSettings();
            settings.MinX = 5000;
            settings.MaxX = 6000;
            settings.MinY = 7000;
            settings.MaxY = 8000;
            settings.NodeRadius = 20;
            layout = new CircleLayout(settings);
        }

        [TestMethod]
        public void CircleLayout_SetsCircleLayout_NodesInRange()
        {
            
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            double minX = double.Parse(g.GetAttributeValue("minX"));
            double minY = double.Parse(g.GetAttributeValue("minY"));
            double maxX = double.Parse(g.GetAttributeValue("maxX"));
            double maxY = double.Parse(g.GetAttributeValue("maxY"));
            foreach (Node n in g.Nodes)
            {
                Assert.IsTrue(n.X >= minX && n.X <= maxX && n.Y >= minY && n.Y <= maxY);
            }
        }

        [TestMethod]
        public void CircleLayout_SetsLayoutAndSavesIntoFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_circle.xml", g));
        }

        [TestMethod]
        public void CircleLayout_SetsLayoutAndSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_circle.gexf", g));
        }
    }
}
