﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for FruchtermanReingoldLayout class.
    /// </summary>
    [TestClass]
    public class FruchtermanReingoldLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private FruchtermanReingoldLayoutSettings settings;
        private FruchtermanReingoldLayout layout;

        public FruchtermanReingoldLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            settings = new FruchtermanReingoldLayoutSettings();
            layout = new FruchtermanReingoldLayout(settings);
        }

        [TestMethod]
        public void FruchtermanReingold_SetsLayoutAndSavesIntoFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_fruchterman.xml", g));
        }

        [TestMethod]
        public void FruchtermanReingold_SetsLayoutAndSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic2.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_fruchterman.gexf", g));
        }

        [TestMethod]
        public void FruchtermanReingold_SetsOnBigGraphSavesIntoFile()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            settings.Iterations = 10;
            layout = new FruchtermanReingoldLayout(settings);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_fruchterman_big.gexf", g));
        }

        [TestMethod]
        public void FruchtermanReingold_SetsOnMidGraphSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_fruchterman_random.gexf", g));
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_fruchterman_mid.gexf", g));
        }

        [TestMethod]
        public void FruchtermanReingold_SetsOnMidGraphSavesIntoGEXF_ChangedSettings()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            settings.Speed = 50;
            settings.Gravity = 100;
            layout = new FruchtermanReingoldLayout(settings);
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_fruchterman_random.gexf", g));
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_fruchterman_mid_speed2.gexf", g));
        }
    }
}
