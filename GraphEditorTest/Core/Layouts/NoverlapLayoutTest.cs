﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts;
using System.IO;
using System.Reflection;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Layouts
{
    /// <summary>
    /// Unit tests for NoverlapLayout class.
    /// </summary>
    [TestClass]
    public class NoverlapLayoutTest
    {
        private string path;
        private CustomXMLParser parser = new CustomXMLParser();
        private NoverlapLayoutSettings settings;
        private NoverlapLayout layout;

        public NoverlapLayoutTest()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
            parser = new CustomXMLParser();
            settings = new NoverlapLayoutSettings();
            settings.NodeRadius = 10;
            layout = new NoverlapLayout(settings);
        }

        [TestMethod]
        public void NoverlapLayout_SetsLayoutAndSavesIntoFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\overlap.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_noverlap.xml", g));
        }

        [TestMethod]
        public void NoverlapLayout_SetsLayoutAndSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\overlap.xml");
            Assert.IsNotNull(g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_noverlap.gexf", g));
        }

        [TestMethod]
        public void NoverlapLayout_SetsOnBigGraphSavesIntoFile()
        {
            CustomXMLWriter writer = new CustomXMLWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_noverlap_big.xml", g));
        }

        [TestMethod]
        public void NoverlapLayout_SetsOnMidGraphSavesIntoGEXF()
        {
            GEXFWriter writer = new GEXFWriter();
            GEXFParser gp = new GEXFParser();
            Graph g = gp.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\celegans.gexf");
            Assert.IsNotNull(g);
            RandomLayout rl = new RandomLayout();
            rl.SetLayout(ref g);
            Assert.IsTrue(layout.SetLayout(ref g));
            Assert.IsTrue(writer.SaveToFile(path + "\\graph_noverlap_mid.gexf", g));
        }
    }
}
