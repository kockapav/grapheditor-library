﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Graphs
{
    /// <summary>
    /// Unit tests for Edge class.
    /// </summary>
    [TestClass]
    public class EdgeTest
    {
        private Edge e;

        public EdgeTest()
        {
            e = new Edge(0, 0, 0);
            Assert.IsNotNull(e);
        }

        [TestMethod]
        public void Edge_GetAndSetEdgeValues()
        {
            Assert.AreEqual(0, e.Source);
            Assert.AreEqual(0, e.Target);
            e.Source = 5;
            Assert.AreEqual(5, e.Source);
            e.Target = 15;
            Assert.AreEqual(15, e.Target);
        }

        [TestMethod]
        public void Edge_GetAndSetAttributes()
        {
            string value;
            Dictionary<string, string> dict = new Dictionary<string, string>();
            Edge e = new Edge(0, 0, 0, dict);
            dict.TryGetValue("key", out value);
            Assert.IsNull(value);
            value = e.GetAttributeValue("key");
            Assert.IsNull(value);
            e.SetAttribute("key", "value");
            value = e.GetAttributeValue("key");
            Assert.AreEqual("value", value);
        }
    }
}
