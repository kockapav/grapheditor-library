﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Graphs
{
    /// <summary>
    /// Unit tests for Graph class.
    /// </summary>
    [TestClass]
    public class GraphTest
    {
        private Graph g;

        public GraphTest()
        {
            g = new Graph(1);
        }

        [TestMethod]
        public void Graph_GetId()
        {
            Assert.AreEqual(1, g.Id);
        }

        [TestMethod]
        public void Graph_GetAndSetAttributes()
        {
            string value;
            value = g.GetAttributeValue("key");
            Assert.IsNull(value);
            g.SetAttribute("key", "value");
            value = g.GetAttributeValue("key");
            Assert.AreEqual("value", value);
        }

        [TestMethod]
        public void Graph_AddAndRemoveNode()
        {
            Node n = new Node(1);
            Assert.IsTrue(g.AddNode(n));
            Assert.IsTrue(g.Nodes.Contains(n));
            Assert.IsTrue(g.RemoveNode(1));
            Assert.IsFalse(g.Nodes.Contains(n));
        }

        [TestMethod]
        public void Graph_AddNodesSameId()
        {
            Node n1 = new Node(1);
            Node n2 = new Node(1);
            g.AddNode(n1);
            Assert.IsFalse(g.AddNode(n2));
        }

        [TestMethod]
        public void Graph_RemoveNonexistingNode()
        {
            Assert.IsFalse(g.RemoveNode(1234));
        }

        [TestMethod]
        public void Graph_AddAndRemoveEdge()
        {
            Node n1 = new Node(10);
            Node n2 = new Node(20);
            Edge e = new Edge(0, 10, 20);
            g.AddNode(n1);
            g.AddNode(n2);
            Console.WriteLine(g.Nodes.Contains(n1));
            Console.WriteLine(n1.Id);
            Console.WriteLine(e.Source);
            Assert.IsTrue(g.AddEdge(e));
            Assert.IsTrue(g.Edges.Contains(e));
            Assert.IsTrue(g.RemoveEdge(10, 20));
            Assert.IsFalse(g.Edges.Contains(e));
        }

        [TestMethod]
        public void Graph_AddEdgeNonexistingNodes()
        {
            Edge e = new Edge(0, 5, 6);
            Assert.IsFalse(g.AddEdge(e));
        }

        [TestMethod]
        public void Graph_RemoveNonexistingEdge()
        {
            Assert.IsFalse(g.RemoveEdge(3, 4));
        }

        [TestMethod]
        public void Graph_RemovingNodeRemovesEdge()
        {
            Node n1 = new Node(1);
            Node n2 = new Node(2);
            Edge e = new Edge(0, 1, 2);
            g.AddNode(n1);
            g.AddNode(n2);
            g.AddEdge(e);
            Assert.IsTrue(g.Nodes.Contains(n1));
            Assert.IsTrue(g.Nodes.Contains(n2));
            Assert.IsTrue(g.Edges.Contains(e));
            Assert.IsTrue(g.RemoveNode(1));
            Assert.IsFalse(g.Nodes.Contains(n1));
            Assert.IsFalse(g.Edges.Contains(e));
            Assert.IsTrue(g.Nodes.Contains(n2));
        }
    }
}
