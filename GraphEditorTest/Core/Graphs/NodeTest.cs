﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Graphs;

namespace GraphEditorTest.Core.Graphs
{
    /// <summary>
    /// Unit tests for Node class.
    /// </summary>
    [TestClass]
    public class NodeTest
    {
        private Node n;

        public NodeTest()
        {
            n = new Node(1);
            Assert.IsNotNull(n);
        }

        [TestMethod]
        public void Node_GetId()
        {
            Assert.AreEqual(1, n.Id);
        }

        [TestMethod]
        public void Node_GetAndSetCoordinates()
        {
            n.X = 10;
            n.Y = 20;
            Assert.AreEqual(10, n.X);
            Assert.AreEqual(20, n.Y);
        }

        [TestMethod]
        public void Node_GetAndSetAttributes()
        {
            string value;
            value = n.GetAttributeValue("key");
            Assert.IsNull(value);
            n.SetAttribute("key", "value");
            value = n.GetAttributeValue("key");
            Assert.AreEqual("value", value);
        }

        [TestMethod]
        public void Node_AddAndRemoveEdge()
        {
            Edge e = new Edge(0, 0, 1);
            n.AddEdge(e);
            Assert.IsTrue(n.Edges.Contains(e));
            n.RemoveEdge(e);
            Assert.IsFalse(n.Edges.Contains(e));
        }
    }
}
