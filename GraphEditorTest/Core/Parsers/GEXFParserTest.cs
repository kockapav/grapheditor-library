﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Parsers;

namespace GraphEditorTest.Core.Parsers
{
    /// <summary>
    /// Unit tests for GEXFParser class
    /// </summary>
    [TestClass]
    public class GEXFParserTest
    {
        private GEXFParser parser;
        private string path;

        public GEXFParserTest()
        {
            parser = new GEXFParser();
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
        }

        [TestMethod]
        public void GEXFParser_LoadGraph()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\basic.gexf");
            Assert.IsNotNull(g);
            Assert.AreEqual(g.Id, 1);
        }

        [TestMethod]
        public void GEXFParser_LoadedGraphHasValues()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\basic.gexf");
            Assert.AreEqual(2, g.Nodes.Count);
            Assert.AreEqual(1, g.Edges.Count);
            Node n = g.Nodes.First();
            Assert.IsNotNull(n);
            Assert.AreEqual("Hello", n.GetAttributeValue("label"));
            Edge e = g.Edges[0];
            Assert.IsNotNull(e);
        }

        [TestMethod]
        public void GEXFParser_LoadsAttributes()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\WebAtlas_EuroSiS.gexf");
            Assert.IsTrue(g.Nodes.Count > 0);
            Assert.IsTrue(g.Edges.Count > 0);
            Node n = g.Nodes.Find(node => node.Id == 10);
            Assert.AreEqual("Czech Republic", n.GetAttributeValue("country"));
        }

        [TestMethod]
        public void GEXFParser_LoadsGraph_VIZPlugin()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\GEXF\\basic-viz.gexf");
            Assert.IsNotNull(g);
            Assert.AreEqual(g.Id, 1);
        }
    }
}
