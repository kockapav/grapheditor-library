﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;

namespace GraphEditorTest.Core.Parsers
{
    /// <summary>
    /// Unit tests for ParserFactory class
    /// </summary>
    [TestClass]
    public class ParserFactoryTest
    {
        [TestMethod]
        public void ParserFactory_ReturnsValidParser()
        {
            ParserFactory factory = new ParserFactory();
            Assert.IsNotNull(factory);
            IGraphParser parser = factory.GetParser(0);
            Assert.IsInstanceOfType(parser, typeof(GEXFParser));
        }

        [TestMethod]
        public void ParserFactory_ReturnsValidParserFromFileName()
        {
            ParserFactory factory = new ParserFactory();
            Assert.IsNotNull(factory);
            IGraphParser parser = factory.GetParserFromPath("random_graph_file.gexf");
            Assert.IsInstanceOfType(parser, typeof(GEXFParser));
        }
    }
}
