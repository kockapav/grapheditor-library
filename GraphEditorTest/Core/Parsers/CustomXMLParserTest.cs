﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Graphs;
using System.IO;
using System.Reflection;

namespace GraphEditorTest.Core.Parsers
{
    /// <summary>
    /// Unit test used for CustomXMLParser class
    /// </summary>
    [TestClass]
    public class CustomXMLParserTest
    {
        private CustomXMLParser parser;
        private string path;

        public CustomXMLParserTest()
        {
            parser = new CustomXMLParser();
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString();
        }

        [TestMethod]
        public void CustomXMLParser_LoadGraph()
        {
            Graph g = parser.LoadFromFile(1, path + "\\Core\\Parsers\\CustomXML\\basic.xml");
            Assert.IsNotNull(g);
            Assert.AreEqual(g.Id, 1);
        }
    }
}
