﻿/*
Copyright 2007 Sandia Corporation. Under the terms of Contract
DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
certain rights in this software.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

 * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
 * Neither the name of Sandia National Laboratories nor the names of
its contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.OpenOrd;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// OpenOrd graph Layout in C#
    /// taken from gephi (java) @author Mathieu Bastian
    /// made single threaded
    /// License -- GPL v3
    /// </summary>
    public class OpenOrdLayout : IGraphLayout
    {
        // Settings
        private OpenOrdLayoutSettings settings;
        private OpenOrdParams param;
        private OpenOrdControl control;

        // Worker attributes
        private DensityGrid densityGrid;
        private bool firstAdd = true;
        private bool fineFirstAdd = true;
        protected Random random;
        private List<Tuple<int, int>> blockedNeighbours;

        #region Getters And Setters
        public OpenOrdLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public OpenOrdLayout()
        {
            settings = new OpenOrdLayoutSettings();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public OpenOrdLayout(LayoutSettings settings)
        {
            if (settings is OpenOrdLayoutSettings)
            {
                this.settings = (OpenOrdLayoutSettings)settings;
            }
            else 
            {
                this.settings = new OpenOrdLayoutSettings(settings);
            }
        }
        #endregion

        /// <summary>
        /// Initializes all variables for OpenOrdLayout
        /// </summary>
        /// <param name="graph"></param>
        public void InitLayout(Graph graph)
        {
            param = OpenOrdParams.DEFAULT;

            //Verify param
            if (param.GetIterationsSum() != 1f)
            {
                param = OpenOrdParams.DEFAULT;
            }

            double highestSimilarity = double.NegativeInfinity;
            foreach (Edge e in graph.Edges)
            {
                if (e.Source != e.Target)
                {
                    string ew = e.GetAttributeValue("weight");
                    double weight;
                    if (ew == null)
                    {
                        weight = 1;
                        e.SetAttribute("weight", "1");
                    }
                    else
                    {
                        weight = double.Parse(ew);
                    }
                    highestSimilarity = Math.Max(highestSimilarity, weight);
                }
            }

            foreach (Node n in graph.Nodes)
            {
                n.X = 0;
                n.Y = 0;
            }

            control = new OpenOrdControl();
            control.EdgeCut = settings.EdgeCut;
            control.RealParm = settings.RealTime;
            control.InitParameters(param, settings.Iterations);
            control.NumNodes = graph.Nodes.Count;
            control.HighestSimilarity = highestSimilarity;

            densityGrid = new DensityGrid();
            densityGrid.Init();
            blockedNeighbours = new List<Tuple<int,int>>();
            random = new Random(Guid.NewGuid().GetHashCode());

            //Add real nodes
            foreach(Node n in graph.Nodes)
            {
                densityGrid.Add(n, control.FineDensity);
            }
        }

        /// <summary>
        /// Sets graph to OpenOrdLayout layout.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            InitLayout(graph);

            while(control.UdpateStage())
            {
                DoWork(ref graph);
            }

            foreach (Node n in graph.Nodes)
            {
                n.RemoveAttribute("subx");
                n.RemoveAttribute("suby");
                n.RemoveAttribute("energy");
            }

            if (settings.NoOverlap)
            {
                NoverlapLayout layout = new NoverlapLayout();
                layout.SetLayout(ref graph);
            }
            else
            {
                GraphUtils.UpdateBorders(ref graph, settings);
            }

            return true;
        }

        private void DoWork(ref Graph graph)
        {
            //Updates nodes
            for(int i=0;i<graph.Nodes.Count;i++)
            {
                UpdateNodePosition(i, ref graph);
            }

            firstAdd = false;
            if (control.FineDensity) {
                fineFirstAdd = false;
            }
        }

        private void UpdateNodePosition(int index, ref Graph graph)
        {
            Node n = graph.Nodes[index];
            double energy0, energy1;
            double updatePos00, updatePos01, updatePos10, updatePos11;
            double jumpLength = 0.01 * control.Temperature;
            densityGrid.Substract(n, firstAdd, fineFirstAdd, control.FineDensity);

            energy0 = GetNodeEnergy(index, graph);
            SolveAnalytic(index, ref graph);
            updatePos00 = n.X;
            updatePos01 = n.Y;
            updatePos10 = updatePos00 + (0.5 - random.NextDouble()) * jumpLength;
            updatePos11 = updatePos01 + (0.5 - random.NextDouble()) * jumpLength;

            n.X = updatePos10;
            n.Y = updatePos11;
            energy1 = GetNodeEnergy(index, graph);

            if (energy0 < energy1) {
                n.X = updatePos00;
                n.Y = updatePos01;
                n.SetAttribute("energy",energy0.ToString());
            } else {
                n.X = updatePos10;
                n.Y = updatePos11;
                n.SetAttribute("energy",energy1.ToString());
            }

            densityGrid.Add(n, control.FineDensity);
        }

        private double GetNodeEnergy(int index, Graph graph)
        {
            double attraction_factor = control.Attraction * control.Attraction
                * control.Attraction * control.Attraction * 2e-2;

            double xDis, yDis;
            double energyDistance;
            double nodeEnergy = 0;

            Node n = graph.Nodes[index];
            foreach(Edge e in n.Edges)
            {
                int id = e.Source == n.Id ? e.Target : e.Source;
                if(id == n.Id) continue;
                if(blockedNeighbours.Find(t => t.Item1 == n.Id && t.Item2 == id) != null) continue;
                double weight = double.Parse(e.GetAttributeValue("weight"));
                Node n2 = graph.Nodes.Find(node => node.Id == id);
                
                xDis = n.X - n2.X;
                yDis = n.Y - n2.Y;

                energyDistance = xDis * xDis + yDis * yDis;
                if (control.Stage < 2) {
                    energyDistance *= energyDistance;
                }

                if (control.Stage == 0) {
                    energyDistance *= energyDistance;
                }

                nodeEnergy += weight * attraction_factor * energyDistance;
            }

            nodeEnergy += densityGrid.GetDensity(n.X, n.Y, control.FineDensity);
            return nodeEnergy;
        }

        private void SolveAnalytic(int index, ref Graph graph)
        {
            double totalWeight = 0;
            double xDis, yDis, xCen = 0, yCen = 0;
            double x = 0, y = 0;
            double damping;
            int neighCnt = 0;

            Node n = graph.Nodes[index];
            if(n.Edges.Count > 0)
            {
                foreach(Edge e in n.Edges)
                {
                    int id = e.Source == n.Id ? e.Target : e.Source;
                    if(id == n.Id) continue;
                    if(blockedNeighbours.Find(t => t.Item1 == n.Id && t.Item2 == id) != null) continue;
                    neighCnt++;
                    double weight = double.Parse(e.GetAttributeValue("weight"));
                    Node n2 = graph.Nodes.Find(node => node.Id == id);

                    totalWeight += weight;
                    x += weight * n2.X;
                    y += weight * n2.Y;
                }

                if (totalWeight > 0) {
                    xCen = x / totalWeight;
                    yCen = y / totalWeight;
                    damping = 1f - control.DampingMult;
                    double posX = damping * n.X + (1 - damping) * xCen;
                    double posY = damping * n.Y + (1 - damping) * yCen;
                    n.X = posX;
                    n.Y = posY;

                    if (control.MinEdges == 99) {
                        return;
                    }
                    if (control.CutEnd >= 39500) {
                        return;
                    }
                }

                double maxLength = 0;
                int maxId = -1;
                if (neighCnt >= control.MinEdges) {
                    foreach(Edge e in n.Edges)
                    {
                        int id = e.Source == n.Id ? e.Target : e.Source;
                        if(id == n.Id) continue;
                        if(blockedNeighbours.Find(t => t.Item1 == n.Id && t.Item2 == id) != null) continue;
                        Node n2 = graph.Nodes.Find(node => node.Id == id);

                        xDis = xCen - n2.X;
                        yDis = yCen - n2.Y;
                        double dis = xDis * xDis + yDis * yDis;
                        dis *= Math.Sqrt(neighCnt);
                        if (dis > maxLength) {
                            maxLength = dis;
                            maxId = id;
                        }
                    }
                }

                if (maxLength > control.CutOffLength && maxId != -1) {
                    blockedNeighbours.Add(new Tuple<int,int>(n.Id, maxId));
                }
            }
        }
    }
}