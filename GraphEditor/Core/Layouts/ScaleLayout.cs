﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// Scale graph layout.
    /// 
    /// Scales up or down whole graph.
    /// </summary>
    public class ScaleLayout : IGraphLayout
    {
        private ScaleLayoutSettings settings;

        #region Getters and Setters
        public ScaleLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScaleLayout()
        {
            settings = new ScaleLayoutSettings();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public ScaleLayout(LayoutSettings settings)
        {
            if (settings is ScaleLayoutSettings)
            {
                this.settings = (ScaleLayoutSettings) settings;
            }
            else 
            {
                this.settings = new ScaleLayoutSettings(settings);
            }
        }
        #endregion

        /// <summary>
        /// Scales graph by @scale.
        /// Scale value higher then 1 expands graph.
        /// Scale value lower then 1 contracts graph.
        /// </summary>
        /// <param name="graph">graph to be scaled</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            double xm = graph.Nodes.Sum(node => node.X) / graph.Nodes.Count;
            double ym = graph.Nodes.Sum(node => node.Y) / graph.Nodes.Count;

            foreach (Node node in graph.Nodes)
            {
                node.X = xm + (node.X - xm) * settings.Scale;
                node.Y = ym + (node.Y - ym) * settings.Scale;
            }

            GraphUtils.UpdateBorders(ref graph, settings);
            return true;
        }
    }
}
