﻿/*
Copyright 2008-2010 Gephi
Authors : Mathieu Jacomy
Website : http://www.gephi.org

This file is part of Gephi.

DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

Copyright 2011 Gephi Consortium. All rights reserved.

The contents of this file are subject to the terms of either the GNU
General Public License Version 3 only ("GPL") or the Common
Development and Distribution License("CDDL") (collectively, the
"License"). You may not use this file except in compliance with the
License. You can obtain a copy of the License at
http://gephi.org/about/legal/license-notice/
or /cddl-1.0.txt and /gpl-3.0.txt. See the License for the
specific language governing permissions and limitations under the
License.  When distributing the software, include this License Header
Notice in each file and include the License files at
/cddl-1.0.txt and /gpl-3.0.txt. If applicable, add the following below the
License Header, with the fields enclosed by brackets [] replaced by
your own identifying information:
"Portions Copyrighted [year] [name of copyright owner]"

If you wish your version of this file to be governed by only the CDDL
or only the GPL Version 3, indicate your decision by adding
"[Contributor] elects to include this software in this distribution
under the [CDDL or GPL Version 3] license." If you do not indicate a
single choice of license, a recipient has the option to distribute
your version of this file under either the CDDL, the GPL Version 3 or
to extend the choice of license to its licensees as provided above.
However, if you add GPL Version 3 code and therefore, elected the GPL
Version 3 license, then the option applies only if the new code is
made subject to such option by the copyright holder.

Contributor(s):

Portions Copyrighted 2011 Gephi Consortium.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Layouts.Data;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// This is a class implementing FruchtermanReingold layout in C#,
    /// as it was implemented (in java) by Mathieu Jacomy in form of a plugin for gephi.
    /// License -- GPL v3
    /// </summary>
    public class FruchtermanReingoldLayout : IGraphLayout
    {
        private double SPEED_DIVISOR = 800;
        private double AREA_MULTIPLICATOR = 10000;
        private FruchtermanReingoldLayoutSettings settings;

        #region Getters and Setters
        public FruchtermanReingoldLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FruchtermanReingoldLayout()
        {
            settings = new FruchtermanReingoldLayoutSettings();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public FruchtermanReingoldLayout(LayoutSettings settings)
        {
            if (settings is FruchtermanReingoldLayoutSettings)
            {
                this.settings = (FruchtermanReingoldLayoutSettings) settings;
            }
            else 
            {
                this.settings = new FruchtermanReingoldLayoutSettings(settings);
            }
        }
        #endregion

        /// <summary>
        /// Sets graph to FruchtemanReingold layout.
        /// Tries to set node coordinates so no nodes will overlap.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            for (int iteration = 0; iteration < settings.Iterations; iteration++)
            {
                Dictionary<Node, NodeLayoutData> nodeData = new Dictionary<Node, NodeLayoutData>();
                foreach (Node node in graph.Nodes) nodeData.Add(node, new NodeLayoutData());

                double maxDisplace = Math.Sqrt(AREA_MULTIPLICATOR * settings.Area) / 10f;
                double k = Math.Sqrt((AREA_MULTIPLICATOR * settings.Area) / (1f + graph.Nodes.Count));

                foreach (Node n1 in graph.Nodes)
                {
                    foreach (Node n2 in graph.Nodes)
                    {
                        if (n1.Id != n2.Id)
                        {
                            double xDist = n1.X - n2.X;
                            double yDist = n1.Y - n2.Y;
                            double dist = (double)Math.Sqrt(xDist * xDist + yDist * yDist);

                            if (dist > 0)
                            {
                                double repulsiveF = k * k / dist;
                                NodeLayoutData layoutData;
                                nodeData.TryGetValue(n1, out layoutData);
                                layoutData.DX += xDist / dist * repulsiveF;
                                layoutData.DY += yDist / dist * repulsiveF;
                            }
                        }
                    }
                }

                foreach (Edge e in graph.Edges)
                {
                    Node nf = graph.Nodes.Find(node => node.Id == e.Source);
                    Node nt = graph.Nodes.Find(node => node.Id == e.Target);

                    double xDist = nf.X - nt.X;
                    double yDist = nf.Y - nt.Y;
                    double dist = Math.Sqrt(xDist * xDist + yDist * yDist);

                    double attractiveF = dist * dist / k;

                    if (dist > 0)
                    {
                        NodeLayoutData sourceLayoutData, targetLayoutData;
                        nodeData.TryGetValue(nf, out sourceLayoutData);
                        nodeData.TryGetValue(nt, out targetLayoutData);
                        sourceLayoutData.DX -= xDist / dist * attractiveF;
                        sourceLayoutData.DY -= yDist / dist * attractiveF;
                        targetLayoutData.DX += xDist / dist * attractiveF;
                        targetLayoutData.DY += yDist / dist * attractiveF;
                    }
                }

                foreach (Node n in graph.Nodes)
                {
                    NodeLayoutData layoutData;
                    nodeData.TryGetValue(n, out layoutData);
                    double d = Math.Sqrt(n.X * n.X + n.Y * n.Y);
                    double gf = 0.01f * k * settings.Gravity * d;
                    layoutData.DX -= gf * n.X / d;
                    layoutData.DY -= gf * n.Y / d;
                }

                foreach (Node n in graph.Nodes)
                {
                    NodeLayoutData layoutData;
                    nodeData.TryGetValue(n, out layoutData);
                    layoutData.DX *= settings.Speed / SPEED_DIVISOR;
                    layoutData.DY *= settings.Speed / SPEED_DIVISOR;
                }

                foreach (Node n in graph.Nodes)
                {
                    NodeLayoutData layoutData;
                    nodeData.TryGetValue(n, out layoutData);
                    double xDist = layoutData.DX;
                    double yDist = layoutData.DY;
                    double dist = Math.Sqrt(layoutData.DX * layoutData.DX + layoutData.DY * layoutData.DY);
                    if (dist > 0)
                    {
                        double limitedDist = Math.Min(maxDisplace * (settings.Speed / SPEED_DIVISOR), dist);
                        n.X = n.X + xDist / dist * limitedDist;
                        n.Y = n.Y + yDist / dist * limitedDist;
                    }
                }      
            }

            GraphUtils.UpdateBorders(ref graph, settings);
            return true;
        }
    }
}
