﻿/*
Copyright 2007 Sandia Corporation. Under the terms of Contract
DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
certain rights in this software.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

 * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
 * Neither the name of Sandia National Laboratories nor the names of
its contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphEditor.Core.Layouts.OpenOrd
{
    /// <summary>
    /// OpenOrdParams class used in OpenOrdLayout
    /// taken from gephi (java) @author Mathieu Bastian
    /// License -- GPL v3
    /// </summary>
    public class OpenOrdParams
    {
        public static OpenOrdParams DEFAULT = new OpenOrdParams(new Stage(0, 2000f, 10f, 1f),
        new Stage(0.25f, 2000f, 10f, 1f),
        new Stage(0.25f, 2000f, 2f, 1f),
        new Stage(0.25f, 2000f, 1f, 0.1f),
        new Stage(0.10f, 250f, 1f, 0.25f),
        new Stage(0.15f, 250f, 0.5f, 0f));
        public static OpenOrdParams COARSEN = new OpenOrdParams(new Stage(0, 2000f, 10f, 1f),
        new Stage(200, 2000f, 2f, 1f),
        new Stage(200, 2000f, 10f, 1f),
        new Stage(200, 2000f, 1f, 0.1f),
        new Stage(50, 250f, 1f, 0.25f),
        new Stage(100, 250f, 0.5f, 0f));
        public static OpenOrdParams COARSEST = new OpenOrdParams(new Stage(0, 2000f, 10f, 1f),
        new Stage(200, 2000f, 2f, 1f),
        new Stage(200, 2000f, 10f, 1f),
        new Stage(200, 2000f, 1f, 0.1f),
        new Stage(200, 250f, 1f, 0.25f),
        new Stage(100, 250f, 0.5f, 0f));
        public static OpenOrdParams REFINE = new OpenOrdParams(new Stage(0, 50f, 0.5f, 0f),
        new Stage(0, 2000f, 2f, 1f),
        new Stage(50, 500f, 0.1f, 0.25f),
        new Stage(50, 200f, 1f, 0.1f),
        new Stage(50, 250f, 1f, 0.25f),
        new Stage(0, 250f, 0.5f, 0f));
        public static OpenOrdParams FINAL = new OpenOrdParams(new Stage(0, 50f, 0.5f, 0f),
        new Stage(0, 2000f, 2f, 1f),
        new Stage(50, 50f, 0.1f, 0.25f),
        new Stage(50, 200f, 1f, 0.1f),
        new Stage(50, 250f, 1f, 0.25f),
        new Stage(25, 250f, 0.5f, 0f));

        public Stage Initial {get; set;}
        public Stage Liquid {get; set;}
        public Stage Expansion {get; set;}
        public Stage Cooldown {get; set;}
        public Stage Crunch {get; set;}
        public Stage Simmer {get; set;}

        private OpenOrdParams(Stage initial, Stage liquid, Stage expansion, Stage cooldown, Stage crunch, Stage simmer) {
            this.Initial = initial;
            this.Liquid = liquid;
            this.Expansion = expansion;
            this.Cooldown = cooldown;
            this.Crunch = crunch;
            this.Simmer = simmer;
        }

        public double GetIterationsSum() {
            return Liquid.Iterations + Expansion.Iterations + Cooldown.Iterations + Crunch.Iterations + Simmer.Iterations;
        }

        public class Stage {

            public double Iterations {get; set;}
            public double Temperature {get; set;}
            public double Attraction {get; set;}
            public double DampingMult {get; set;}

            public Stage(double iterations, double temperature, double attraction, double dampingMult) {
                this.Iterations = iterations;
                this.Temperature = temperature;
                this.Attraction = attraction;
                this.DampingMult = dampingMult;
            }

            public int GetIterationsTotal(int totalIterations) {
                return (int) (Iterations * totalIterations);
            }

            public int GetIterationsPercentage() {
                return (int) (Iterations * 100f);
            }
        }
    }
}
