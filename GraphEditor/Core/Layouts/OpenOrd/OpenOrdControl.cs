﻿/*
Copyright 2007 Sandia Corporation. Under the terms of Contract
DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
certain rights in this software.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

 * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
 * Neither the name of Sandia National Laboratories nor the names of
its contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphEditor.Core.Layouts.OpenOrd
{
    /// <summary>
    /// OpenOrdControl class used by OpenOrdLayout
    /// taken from gephi (java) @author Mathieu Bastian
    /// License -- GPL v3
    /// </summary>
    public class OpenOrdControl
    {
        //Settings
        private int STAGE;
        private int iterations;
        private double temperature;
        private double attraction;
        private double dampingMult;
        private double minEdges;
        private double cutEnd;
        private double cutLengthEnd;
        private double cutOffLength;
        private double cutRate;
        private bool fineDensity;
        //Vars
        private double edgeCut;
        private double realParm;
        //Exec
        private int numNodes;
        private double highestSimilarity;
        private int realIterations;
        private bool realFixed;
        private int totIterations;
        private int totExpectedIterations;
        private OpenOrdParams parameters;

        #region Getters and Setters
        public bool IsRealFixed
        {
            get { return realFixed; }
        }

        public double HighestSimilarity
        {
            get { return highestSimilarity; }
            set { highestSimilarity = value; }
        }

        public int NumNodes
        {
            get { return numNodes; }
            set { numNodes = value; }
        }

        public double EdgeCut
        {
            get { return edgeCut; }
            set { edgeCut = value; }
        }

        public double RealParm
        {
            get { return realParm; }
            set { realParm = value; }
        }

        public int Stage
        {
            get { return STAGE; }
        }

        public double Attraction
        {
            get { return attraction; }
        }

        public double Temperature
        {
            get { return temperature; }
        }

        public double DampingMult
        {
            get { return dampingMult; }
        }

        public double MinEdges
        {
            get { return minEdges; }
        }

        public double CutEnd
        {
            get { return cutEnd; }
        }

        public double CutOffLength
        {
            get { return cutOffLength; }
        }

        public bool FineDensity
        {
            get { return fineDensity; }
        }
        #endregion

        public void InitParameters(OpenOrdParams parameters, int totalIterations) {
            this.parameters = parameters;
            STAGE = 0;
            iterations = 0;
            InitStage(parameters.Initial);
            minEdges = 20;
            fineDensity = false;
 

            cutEnd = cutLengthEnd = 40000 * (1 - edgeCut);
            if (cutLengthEnd <= 1) {
                cutLengthEnd = 1;
            }

            double cutLengthStart = 4 * cutLengthEnd;

            cutOffLength = cutLengthStart;
            cutRate = (cutLengthStart - cutLengthEnd) / 400;

            totExpectedIterations = totalIterations;

            int fullCompIters = totExpectedIterations + 3;

            if (realParm < 0) {
                realIterations = (int) realParm;
            } else if (realParm == 1) {
                realIterations = fullCompIters + parameters.Simmer.GetIterationsTotal(totalIterations) + 100;
            } else {
                realIterations = (int) (realParm * fullCompIters);
            }

            if (realIterations > 0) {
                realFixed = true;
            } else {
                realFixed = false;
            }
        }

        private void InitStage(OpenOrdParams.Stage stage) {
            temperature = stage.Temperature;
            attraction = stage.Attraction;
            dampingMult = stage.DampingMult;
        }

        public bool UdpateStage() {
            int MIN = 1;

            totIterations++;
            if (totIterations >= realIterations) {
                realFixed = false;
            }

            if (STAGE == 0) {
                if (iterations < parameters.Liquid.GetIterationsTotal(totExpectedIterations)) {
                    InitStage(parameters.Liquid);
                    iterations++;
                } else {
                    InitStage(parameters.Expansion);
                    iterations = 0;
                    STAGE = 1;
                }
            }

            if (STAGE == 1) {
                if (iterations < parameters.Expansion.GetIterationsTotal(totExpectedIterations)) {
                    // Play with vars
                    if (attraction > 1) {
                        attraction -= .05;
                    }
                    if (minEdges > 12) {
                        minEdges -= .05;
                    }
                    cutOffLength -= cutRate;
                    if (dampingMult > .1) {
                        dampingMult -= .005;
                    }
                    iterations++;
                } else {
                    STAGE = 2;
                    minEdges = 12;
                    InitStage(parameters.Cooldown);
                    iterations = 0;
                }
            } else if (STAGE == 2) {
                if (iterations < parameters.Cooldown.GetIterationsTotal(totExpectedIterations)) {
                    // Reduce temperature
                    if (temperature > 50) {
                        temperature -= 10;
                    }

                    // Reduce cut length
                    if (cutOffLength > cutLengthEnd) {
                        cutLengthEnd -= cutRate * 2;
                    }
                    if (minEdges > MIN) {
                        minEdges -= .2;
                    }
                    //min_edges = 99;
                    iterations++;
                } else {
                    cutOffLength = cutLengthEnd;
                    minEdges = MIN;
                    //min_edges = 99; // In other words: no more cutting
                    STAGE = 3;
                    iterations = 0;
                    InitStage(parameters.Crunch);
                }
            } else if (STAGE == 3) {
                if (iterations < parameters.Crunch.GetIterationsTotal(totExpectedIterations)) {
                    iterations++;
                } else {
                    iterations = 0;
                    InitStage(parameters.Simmer);
                    minEdges = 99;
                    fineDensity = true;
                    STAGE = 5;
                }
            } else if (STAGE == 5) {
                if (iterations < parameters.Simmer.GetIterationsTotal(totExpectedIterations)) {
                    if (temperature > 50) {
                        temperature -= 2;
                    }
                    iterations++;
                } else {
                    STAGE = 6;
                }
            } else if (STAGE == 6) {
                return false;
            }

            return true;
        }
    }
}
