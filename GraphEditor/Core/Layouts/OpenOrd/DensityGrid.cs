﻿/*
Copyright 2007 Sandia Corporation. Under the terms of Contract
DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
certain rights in this software.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

 * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
 * Neither the name of Sandia National Laboratories nor the names of
its contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;
using System.Xml;

namespace GraphEditor.Core.Layouts.OpenOrd
{
    /// <summary>
    /// DensityGrid class used by OpenOrd Layout
    /// taken from gephi (java) @author Mathieu Bastian
    /// License -- GPL v3
    /// </summary>
    public class DensityGrid
    {
        private const int GRID_SIZE = 1000;         // size of Density grid
        private const double VIEW_SIZE = 4000;       // actual physical size of layout plane
        private const int RADIUS = 10;              // radius for density fall-off:
        private const int HALF_VIEW = 2000;
        private const double VIEW_TO_GRID = 0.25f;
        private double[,] density;
        private double[,] fallOff;
        private LinkedList<Node>[,] bins;

        public void Init() {
            density = new double[GRID_SIZE, GRID_SIZE];
            fallOff = new double[RADIUS * 2 + 1,RADIUS * 2 + 1];
            bins = new LinkedList<Node>[GRID_SIZE, GRID_SIZE];

            for (int i = -RADIUS; i <= RADIUS; i++) {
                for (int j = -RADIUS; j <= RADIUS; j++) {
                    fallOff[i + RADIUS, j + RADIUS] = (double) ((RADIUS - Math.Abs((double) i)) / RADIUS)
                            * (double) ((RADIUS - Math.Abs((double) j)) / RADIUS);
                }
            }
        }

        public double GetDensity(double nX, double nY, bool fineDensity) {
            int xGrid, yGrid;
            double xDist, yDist, distance, density = 0;
            int boundary = 10;      // boundary around plane

            xGrid = (int) ((nX + HALF_VIEW + .5) * VIEW_TO_GRID);
            yGrid = (int) ((nY + HALF_VIEW + .5) * VIEW_TO_GRID);

            // Check for edges of density grid (10000 is arbitrary high density)
            if (xGrid > GRID_SIZE - boundary || xGrid < boundary) {
                return 10000;
            }
            if (yGrid > GRID_SIZE - boundary || yGrid < boundary) {
                return 10000;
            }

            if (fineDensity) {
                for (int i = yGrid - 1; i <= yGrid + 1; i++) {
                    for (int j = xGrid - 1; j <= xGrid + 1; j++) {
                        LinkedList<Node> deque = bins[i,j];
                        if (deque != null) {
                            foreach (Node bi in deque) {
                                xDist = nX - bi.X;
                                yDist = nY - bi.Y;
                                distance = xDist * xDist + yDist * yDist;
                                density += 1e-4 / (distance + 1e-50);
                            }
                        }
                    }
                }
            } else {
                density = this.density[yGrid,xGrid];
                density *= density;
            }
            return density;
        }

        public void Add(Node n, bool fineDensity) {
            if (fineDensity) {
                FineAdd(n);
            } else {
                Add(n);
            }
        }

        public void Substract(Node n, bool firstAdd, bool fineFirstAdd, bool fineDensity) {
            if (fineDensity && !fineFirstAdd) {
                FineSubstract(n);
            } else if (!firstAdd) {
                Substract(n);
            }
        }

        private void Substract(Node n) {
            int xGrid, yGrid, diam;

            xGrid = Convert.ToInt32((double.Parse(n.GetAttributeValue("subx")) + HALF_VIEW + 0.5) * VIEW_TO_GRID);
            yGrid = Convert.ToInt32((double.Parse(n.GetAttributeValue("suby")) + HALF_VIEW + 0.5) * VIEW_TO_GRID);
            xGrid -= RADIUS;
            yGrid -= RADIUS;
            diam = 2 * RADIUS;

            for (int i = 0; i <= diam; i++) {
                int oldXGrid = xGrid;
                for (int j = 0; j <= diam; j++) {
                    density[yGrid, xGrid] -= fallOff[i, j];
                    xGrid++;
                }
                yGrid++;
                xGrid = oldXGrid;
            }
        }

        private void Add(Node n) {
            int xGrid, yGrid, diam;

            xGrid = (int) ((n.X + HALF_VIEW + .5) * VIEW_TO_GRID);
            yGrid = (int) ((n.Y + HALF_VIEW + .5) * VIEW_TO_GRID);

            n.SetAttribute("subx", n.X.ToString());
            n.SetAttribute("suby", n.Y.ToString());

            xGrid -= RADIUS;
            yGrid -= RADIUS;
            diam = 2 * RADIUS;

            if ((xGrid + RADIUS >= GRID_SIZE) || (xGrid < 0)
                    || (yGrid + RADIUS >= GRID_SIZE) || (yGrid < 0)) {
                        throw new Exception("Error: Exceeded density grid.");
            }

            for (int i = 0; i <= diam; i++) {
                int oldXGrid = xGrid;
                for (int j = 0; j <= diam; j++) {
                    density[yGrid, xGrid] += fallOff[i, j];
                    xGrid++;
                }
                yGrid++;
                xGrid = oldXGrid;
            }
        }

        private void FineSubstract(Node n) {
            int xGrid, yGrid;

            xGrid = Convert.ToInt32((double.Parse(n.GetAttributeValue("subx")) + HALF_VIEW + 0.5) * VIEW_TO_GRID);
            yGrid = Convert.ToInt32((double.Parse(n.GetAttributeValue("suby")) + HALF_VIEW + 0.5) * VIEW_TO_GRID);
            LinkedList<Node> deque = bins[yGrid, xGrid];
            if (deque != null && deque.Count > 0) {
                deque.RemoveFirst();
            }
        }

        private void FineAdd(Node n) {
            int xGrid, yGrid;

            xGrid = (int) ((n.X + HALF_VIEW + .5) * VIEW_TO_GRID);
            yGrid = (int) ((n.Y + HALF_VIEW + .5) * VIEW_TO_GRID);

            n.SetAttribute("subx", n.X.ToString());
            n.SetAttribute("suby", n.Y.ToString());
            LinkedList<Node> deque = bins[yGrid,xGrid];
            if (deque == null) {
                deque = new LinkedList<Node>();
                bins[yGrid,xGrid] = deque;
            }
            deque.AddLast(n);
        }

        public static double GetViewSize() {
            return (VIEW_SIZE * 0.8) - (RADIUS / 0.25) * 2;
        }
    }
}
