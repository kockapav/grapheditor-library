﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// Rotate graph layout.
    /// 
    /// Rotates whole graph around origin point
    /// </summary>
    public class RotateLayout : IGraphLayout
    {
        private RotateLayoutSettings settings;

        #region Getters and Setters
        public RotateLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RotateLayout()
        {
            settings = new RotateLayoutSettings();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public RotateLayout(LayoutSettings settings)
        {
            if (settings is RotateLayoutSettings)
            {
                this.settings = (RotateLayoutSettings) settings;
            }
            else 
            {
                this.settings = new RotateLayoutSettings(settings);
            }
        }
        #endregion

        /// <summary>
        /// Rotates graph by @angle degrees in clockwise or counter-clockwise direction
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            int cw = settings.Clockwise ? -1 : 1;
            double sinD = Math.Sin(cw * settings.Angle * Math.PI / 180);
            double cosD = Math.Cos(cw * settings.Angle * Math.PI / 180);

            foreach (Node node in graph.Nodes)
            {
                double dx = node.X - settings.OriginX;
                double dy = node.Y - settings.OriginY;
                node.X = settings.OriginX + dx * cosD - dy * sinD;
                node.Y = settings.OriginY + dx * sinD + dy * cosD;
            }

            UpdateBorders(ref graph);
            return true;
        }

        private void UpdateBorders(ref Graph graph)
        {
            double minX = settings.MinX;
            double maxX = settings.MaxX;
            double minY = settings.MinY;
            double maxY = settings.MaxY;

            foreach (Node n in graph.Nodes)
            {
                minX = Math.Min(n.X, minX);
                maxX = Math.Max(n.X, maxX);
                minY = Math.Min(n.Y, minY);
                maxY = Math.Max(n.Y, maxY);
            }

            graph.SetAttribute("minX", minX.ToString());
            graph.SetAttribute("maxX", maxX.ToString());
            graph.SetAttribute("minY", minY.ToString());
            graph.SetAttribute("maxY", maxY.ToString());
        }
    }
}
