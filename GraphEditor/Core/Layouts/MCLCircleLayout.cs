﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Clusterings;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// Circle graph layout using Markov Clustering Algorithm.
    /// 
    /// Creates a circle from clusters, every cluster is a nested circle.
    /// </summary>
    public class MCLCircleLayout : IGraphLayout
    {
        private MCLCircleLayoutSettings settings;
        private MarkovClustering clustering;

        #region Getters and Setters
        public MCLCircleLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MCLCircleLayout()
        {
            settings = new MCLCircleLayoutSettings();
            clustering = new MarkovClustering();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public MCLCircleLayout(LayoutSettings settings)
        {
            if (settings is MCLCircleLayoutSettings)
            {
                this.settings = (MCLCircleLayoutSettings)settings;
            }
            else 
            {
                this.settings = new MCLCircleLayoutSettings(settings);
            }
            clustering = new MarkovClustering();
        }
        #endregion

        /// <summary>
        /// Sets graph to MCLCircleLayout.
        /// Sets node coordinates in graph for them to form a circle.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            // clusterize graph using mcl algorithm
            if (!clustering.ClusterizeGraph(ref graph, settings.ExpansionsCnt, settings.InflationsCnt)) return false;

            int clCnt = int.Parse(graph.GetAttributeValue("clusters"));
            List<Node>[] clusterNodes = new List<Node>[clCnt];
            for (int i = 0; i < clCnt; i++) clusterNodes[i] = new List<Node>();
            double[] clAngles = new double[clCnt];
            double[] clRadiuses = new double[clCnt];

            foreach(Node n in graph.Nodes)
            {
                int clIndex = int.Parse(n.GetAttributeValue("cluster"));
                clusterNodes[clIndex].Add(n);
            }

            for (int i = 0; i < clCnt; i++)
            {
                clAngles[i] = -2 * Math.PI / clusterNodes[i].Count;
                clRadiuses[i] = 1.5 * settings.NodeRadius * clusterNodes[i].Count / Math.PI;
            }

            double angle = -2 * Math.PI / clCnt;
            double radius = 3 * clRadiuses.Sum() / Math.PI;

            for (int i = 0; i < clCnt; i++)
            {
                double clOriginX = radius * (Math.Cos((angle * i) + (Math.PI / 2))) + settings.OriginX;
                double clOriginY = radius * (Math.Sin((angle * i) + (Math.PI / 2))) + settings.OriginY;

                int index = 0;
                foreach (Node n in clusterNodes[i])
                {
                    n.X = clRadiuses[i] * (Math.Cos((clAngles[i] * index) + (Math.PI / 2))) + clOriginX;
                    n.Y = clRadiuses[i] * (Math.Sin((clAngles[i] * index) + (Math.PI / 2))) + clOriginY;
                    index++;
                }
            }

            GraphUtils.UpdateBorders(ref graph, settings);
            graph.SetAttribute("nodeRadius", settings.NodeRadius.ToString());
            graph.SetAttribute("originX", settings.OriginX.ToString());
            graph.SetAttribute("originY", settings.OriginY.ToString());
            return true;
        }
    }
}
