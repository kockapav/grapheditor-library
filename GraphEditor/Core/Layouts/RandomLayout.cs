﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Layouts.Settings;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// Random graph layout.
    /// 
    /// Changes nodes x, y coordinates in graph to random values.
    /// Range of random values can be specified using LayoutSettings.
    /// </summary>
    public class RandomLayout : IGraphLayout
    {
        private LayoutSettings settings;
        private Random random;

        #region Getters and Setters
        public LayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// Sets min and max values to default.
        /// </summary>
        public RandomLayout()
        {
            settings = new LayoutSettings();
            random = new Random(DateTime.Now.Millisecond);
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public RandomLayout(LayoutSettings settings)
        {
            this.settings = settings;
            random = new Random(DateTime.Now.Millisecond);
        }
        #endregion

        #region Layout setting methods
        /// <summary>
        /// Sets graph layout to RandomLayout.
        /// 
        /// Node coordinates are generated randomly based on min and max values.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            double coefX = settings.MaxX - settings.MinX;
            double diffX = 0 - settings.MinX;
            double coefY = settings.MaxY - settings.MinY;
            double diffY = 0 - settings.MinY;

            foreach (Node node in graph.Nodes)
            {
                node.X = random.NextDouble() * coefX - diffX;
                node.Y = random.NextDouble() * coefY - diffY;
            }

            GraphUtils.UpdateBorders(ref graph, settings);
            return true;
        }
        #endregion
    }
}
