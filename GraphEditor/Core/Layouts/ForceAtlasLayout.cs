﻿/*
Copyright 2008-2010 Gephi
Authors : Mathieu Jacomy
Website : http://www.gephi.org

This file is part of Gephi.

DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

Copyright 2011 Gephi Consortium. All rights reserved.

The contents of this file are subject to the terms of either the GNU
General Public License Version 3 only ("GPL") or the Common
Development and Distribution License("CDDL") (collectively, the
"License"). You may not use this file except in compliance with the
License. You can obtain a copy of the License at
http://gephi.org/about/legal/license-notice/
or /cddl-1.0.txt and /gpl-3.0.txt. See the License for the
specific language governing permissions and limitations under the
License.  When distributing the software, include this License Header
Notice in each file and include the License files at
/cddl-1.0.txt and /gpl-3.0.txt. If applicable, add the following below the
License Header, with the fields enclosed by brackets [] replaced by
your own identifying information:
"Portions Copyrighted [year] [name of copyright owner]"

If you wish your version of this file to be governed by only the CDDL
or only the GPL Version 3, indicate your decision by adding
"[Contributor] elects to include this software in this distribution
under the [CDDL or GPL Version 3] license." If you do not indicate a
single choice of license, a recipient has the option to distribute
your version of this file under either the CDDL, the GPL Version 3 or
to extend the choice of license to its licensees as provided above.
However, if you add GPL Version 3 code and therefore, elected the GPL
Version 3 license, then the option applies only if the new code is
made subject to such option by the copyright holder.

Contributor(s):

Portions Copyrighted 2011 Gephi Consortium.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Layouts.Data;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// This is a class implementing ForceAtlas layout in C#,
    /// as it was implemented (in java) by Mathieu Jacomy in form of a plugin for gephi.
    /// License -- GPL v3
    /// </summary>
    public class ForceAtlasLayout : IGraphLayout
    {
        private ForceAtlasLayoutSettings settings;
        private Dictionary<Node, NodeLayoutData> nodeData;

        #region Getters and Setters
        public ForceAtlasLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ForceAtlasLayout()
        {
            settings = new ForceAtlasLayoutSettings();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public ForceAtlasLayout(LayoutSettings settings)
        {
            if (settings is ForceAtlasLayoutSettings)
            {
                this.settings = (ForceAtlasLayoutSettings)settings;
            }
            else 
            {
                this.settings = new ForceAtlasLayoutSettings(settings);
            }
        }
        #endregion

        /// <summary>
        /// Sets graph to ForceAtlasLayoutSettings layout.
        /// Tries to set node coordinates so no nodes will overlap.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            for (int iteration = 0; iteration < settings.Iterations; iteration++)
            {
                if (nodeData == null)
                {
                    nodeData = new Dictionary<Node, NodeLayoutData>();
                    foreach (Node node in graph.Nodes)
                    {
                        nodeData.Add(node, new NodeLayoutData());
                    }
                }

                foreach (Node node in graph.Nodes)
                {
                    NodeLayoutData ndata;
                    nodeData.TryGetValue(node, out ndata);
                    ndata.OldDX = ndata.DX;
                    ndata.OldDY = ndata.DY;
                    ndata.DX *= settings.Inertia;
                    ndata.DY *= settings.Inertia;
                }

                foreach (Node n1 in graph.Nodes)
                {
                    NodeLayoutData n1data, n2data;
                    nodeData.TryGetValue(n1, out n1data);
                    foreach (Node n2 in graph.Nodes)
                    {
                        if (n1.Id != n2.Id)
                        {
                            nodeData.TryGetValue(n2, out n2data);
                            ForceVectorUtils.FcBiRepulsor_noCollide(n1, n2, ref n1data, ref n2data, settings, settings.RepulsionStrength * (1 + n1.Edges.Count) * (1 + n2.Edges.Count));
                        }
                    }
                }

                if (settings.OutboundAttractionDistribution)
                {
                    foreach (Edge e in graph.Edges)
                    {
                        Node nf = graph.Nodes.Find(node => node.Id == e.Source);
                        Node nt = graph.Nodes.Find(node => node.Id == e.Target);
                        NodeLayoutData nfdata, ntdata;
                        nodeData.TryGetValue(nf, out nfdata);
                        nodeData.TryGetValue(nt, out ntdata);
                        string w = e.GetAttributeValue("weight");
                        double weight = w == null ? settings.EdgeWeight : double.Parse(w);
                        ForceVectorUtils.FcBiAttractor_noCollide(nf, nt, ref nfdata, ref ntdata, settings, weight * settings.AttractionStrength / (1 + nf.Edges.Count));
                    }
                }
                else
                {
                    foreach (Edge e in graph.Edges)
                    {
                        Node nf = graph.Nodes.Find(node => node.Id == e.Source);
                        Node nt = graph.Nodes.Find(node => node.Id == e.Target);
                        NodeLayoutData nfdata, ntdata;
                        nodeData.TryGetValue(nf, out nfdata);
                        nodeData.TryGetValue(nt, out ntdata);
                        string w = e.GetAttributeValue("weight");
                        double weight = w == null ? settings.EdgeWeight : double.Parse(w);
                        ForceVectorUtils.FcBiAttractor_noCollide(nf, nt, ref nfdata, ref ntdata, settings, weight * settings.AttractionStrength);
                    }
                }

                // gravity
                foreach (Node n in graph.Nodes)
                {
                    double nx = n.X;
                    double ny = n.Y;
                    double d = 0.0001 + Math.Sqrt(nx * nx + ny * ny);
                    double gf = 0.0001 * settings.Gravity * d;
                    NodeLayoutData ndata;
                    nodeData.TryGetValue(n, out ndata);
                    ndata.DX -= gf * nx / d;
                    ndata.DY -= gf * ny / d;
                }

                if (settings.FreezeBalance)
                {
                    foreach (Node n in graph.Nodes)
                    {
                        NodeLayoutData ndata;
                        nodeData.TryGetValue(n, out ndata);
                        ndata.DX *= settings.Speed * 10;
                        ndata.DY *= settings.Speed * 10;
                    }
                }
                else
                {
                    foreach (Node n in graph.Nodes)
                    {
                        NodeLayoutData ndata;
                        nodeData.TryGetValue(n, out ndata);
                        ndata.DX *= settings.Speed;
                        ndata.DY *= settings.Speed;
                    }
                }

                foreach (Node n in graph.Nodes)
                {
                    NodeLayoutData ndata;
                    nodeData.TryGetValue(n, out ndata);
                    double d = 0.0001 + Math.Sqrt(ndata.DX * ndata.DX + ndata.DY * ndata.DY);
                    double ratio;
                    if (settings.FreezeBalance)
                    {
                        ndata.Freeze = settings.Inertia * ndata.Freeze + (1 - settings.Inertia) * 0.1 * settings.FreezeStrength * (Math.Sqrt(Math.Sqrt((ndata.OldDX - ndata.DX) * (ndata.OldDX - ndata.DX) + (ndata.OldDY - ndata.DY) * (ndata.OldDY - ndata.DY))));
                        ratio = (float)Math.Min((d / (d * (1f + ndata.Freeze))), settings.MaxDisplacement / d);
                    }
                    else
                    {
                        ratio = (float)Math.Min(1, settings.MaxDisplacement / d);
                    }
                    ndata.DX *= ratio / settings.Cooling;
                    ndata.DY *= ratio / settings.Cooling;
                    n.X += ndata.DX;
                    n.Y += ndata.DY;
                }
            }

            GraphUtils.UpdateBorders(ref graph, settings);

            return true;
        }
    }
}
