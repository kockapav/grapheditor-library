﻿/*
Copyright 2008-2010 Gephi
Authors : Helder Suzuki <heldersuzuki@gephi.org>
Website : http://www.gephi.org

This file is part of Gephi.

DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

Copyright 2011 Gephi Consortium. All rights reserved.

The contents of this file are subject to the terms of either the GNU
General Public License Version 3 only ("GPL") or the Common
Development and Distribution License("CDDL") (collectively, the
"License"). You may not use this file except in compliance with the
License. You can obtain a copy of the License at
http://gephi.org/about/legal/license-notice/
or /cddl-1.0.txt and /gpl-3.0.txt. See the License for the
specific language governing permissions and limitations under the
License.  When distributing the software, include this License Header
Notice in each file and include the License files at
/cddl-1.0.txt and /gpl-3.0.txt. If applicable, add the following below the
License Header, with the fields enclosed by brackets [] replaced by
your own identifying information:
"Portions Copyrighted [year] [name of copyright owner]"

If you wish your version of this file to be governed by only the CDDL
or only the GPL Version 3, indicate your decision by adding
"[Contributor] elects to include this software in this distribution
under the [CDDL or GPL Version 3] license." If you do not indicate a
single choice of license, a recipient has the option to distribute
your version of this file under either the CDDL, the GPL Version 3 or
to extend the choice of license to its licensees as provided above.
However, if you add GPL Version 3 code and therefore, elected the GPL
Version 3 license, then the option applies only if the new code is
made subject to such option by the copyright holder.

Contributor(s):

Portions Copyrighted 2011 Gephi Consortium.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Data
{
    /// <summary>
    /// QuadTree class used in YifanHu Layout
    /// as it was implemented in gephi, rewritten in C#
    /// License -- GPL v3
    /// </summary>
    public class QuadTree
    {
        public const double eps = (float) 1e-6;
        private enum BehaviourIds {FirstAdd, SecondAdd, RootAdd, LeafAdd};
        private double posX;
        private double posY;
        private double size;
        private double centerMassX;  // X and Y position of the center of mass
        private double centerMassY;
        private int mass;  // Mass of this tree (the number of nodes it contains)
        private int maxLevel;
        private int behaviurId;
        private List<QuadTree> children;
        private bool isLeaf;

        #region Getters and Setters
        public double Size 
        {
            get { return size; }
            set { size = value; }
        }

        public List<QuadTree> Children 
        {
            get { return children; }
        }

        public double X
        {
            get { return centerMassX; }
            set { centerMassX = value; }
        }

        public double Y
        {
            get { return centerMassY; }
            set { centerMassY = value; }
        }

        public int Mass
        {
            get { return mass; }
            set { mass = value; }
        }

        public bool IsLeaf
        {
            get { return isLeaf; }
        }

        public int BehaviourId
        {
            get { return behaviurId; }
            set { behaviurId = value; }
        }
        #endregion

        #region Constructors
        public QuadTree(double posX, double posY, double size, int maxLevel)
        {
            this.posX = posX;
            this.posY = posY;
            this.size = size;
            this.maxLevel = maxLevel;
            this.isLeaf = true;
            mass = 0;
            behaviurId = (int) BehaviourIds.FirstAdd;
        }
        #endregion

        public static QuadTree BuildTree(Graph graph, int maxLevel)
        {
            double minX = double.MaxValue;
            double minY = double.MaxValue;
            double maxX = double.MinValue;
            double maxY = double.MinValue;

            foreach(Node node in graph.Nodes)
            {
                minX = Math.Min(minX, node.X);
                maxX = Math.Max(maxX, node.X);
                minY = Math.Min(minY, node.Y);
                maxY = Math.Max(maxY, node.Y);
            }

            double size = Math.Max(maxY - minY, maxX - minX);
            QuadTree tree = new QuadTree(minX, minY, size, maxLevel);
            foreach(Node node in graph.Nodes)
            {
                tree.AddNode(node);
            }
            return tree;
        }

        private void DivideTree()
        {
            double childSize = size / 2;
            children = new List<QuadTree>();
            children.Add(new QuadTree(posX + childSize, posY + childSize,
                                      childSize, maxLevel - 1));
            children.Add(new QuadTree(posX, posY + childSize,
                                      childSize, maxLevel - 1));
            children.Add(new QuadTree(posX, posY, childSize, maxLevel - 1));
            children.Add(new QuadTree(posX + childSize, posY,
                                      childSize, maxLevel - 1));
            isLeaf = false;
        }

        private bool AddToChildren(Node node)
        {
            foreach (QuadTree q in children) {
                if (q.AddNode(node)) {
                    return true;
                }
            }
            return false;
        }

        private void AssimilateNode(Node node)
        {
            centerMassX = (mass * centerMassX + node.X) / (mass + 1);
            centerMassY = (mass * centerMassY + node.Y) / (mass + 1);
            mass++;
        }

        public bool AddNode(Node node)
        {
            if (posX <= node.X && node.X <= posX + size &&
                posY <= node.Y && node.Y <= posY + size) {
                return Add(node);
            } else {
                return false;
            }
        }

        private bool Add(Node node)
        {
            switch(behaviurId)
            {
                case (int) BehaviourIds.FirstAdd:
                    return FirstAdd(node);
                case (int) BehaviourIds.SecondAdd:
                    break;
                case (int) BehaviourIds.RootAdd:
                    break;
                case (int) BehaviourIds.LeafAdd:
                    break;
            }

            return false;
        }

        private bool FirstAdd(Node node)
        {
            mass = 1;
            centerMassX = node.X;
            centerMassY = node.Y;

            if (maxLevel == 0) {
                behaviurId = (int) BehaviourIds.LeafAdd;
            } else {
                behaviurId = (int) BehaviourIds.SecondAdd;
            }

            return true;
        }

        private bool SecondAdd(Node node)
        {
            DivideTree();
            behaviurId = (int) BehaviourIds.RootAdd;
            AddToChildren(new Node(0, centerMassX, centerMassY, null));
            return AddNode(node);
        }

        private bool LeafAdd(Node node)
        {
            AssimilateNode(node);
            return true;
        }

        private bool RootAdd(Node node)
        {
            AssimilateNode(node);
            return AddToChildren(node);
        }
    }
}