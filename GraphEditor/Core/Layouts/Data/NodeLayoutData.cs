﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Data
{
    /// <summary>
    /// Node layout data class, used for setting node neighbours and forces
    /// </summary>
    public class NodeLayoutData
    {
        public HashSet<Node> Neighbours { get; set; }
        public double DX { get; set; }
        public double DY { get; set; }
        public double OldDX { get; set; }
        public double OldDY { get; set; }
        public double Freeze { get; set; }

        public NodeLayoutData()
        {
            Neighbours = new HashSet<Node>();
            DX = 0;
            DY = 0;
            OldDX = 0;
            OldDY = 0;
            Freeze = 0;
        }
    }
}
