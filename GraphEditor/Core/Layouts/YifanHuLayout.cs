﻿/*
Copyright 2008-2010 Gephi
Authors : Helder Suzuki <heldersuzuki@gephi.org>
Website : http://www.gephi.org

This file is part of Gephi.

DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

Copyright 2011 Gephi Consortium. All rights reserved.

The contents of this file are subject to the terms of either the GNU
General Public License Version 3 only ("GPL") or the Common
Development and Distribution License("CDDL") (collectively, the
"License"). You may not use this file except in compliance with the
License. You can obtain a copy of the License at
http://gephi.org/about/legal/license-notice/
or /cddl-1.0.txt and /gpl-3.0.txt. See the License for the
specific language governing permissions and limitations under the
License.  When distributing the software, include this License Header
Notice in each file and include the License files at
/cddl-1.0.txt and /gpl-3.0.txt. If applicable, add the following below the
License Header, with the fields enclosed by brackets [] replaced by
your own identifying information:
"Portions Copyrighted [year] [name of copyright owner]"

If you wish your version of this file to be governed by only the CDDL
or only the GPL Version 3, indicate your decision by adding
"[Contributor] elects to include this software in this distribution
under the [CDDL or GPL Version 3] license." If you do not indicate a
single choice of license, a recipient has the option to distribute
your version of this file under either the CDDL, the GPL Version 3 or
to extend the choice of license to its licensees as provided above.
However, if you add GPL Version 3 code and therefore, elected the GPL
Version 3 license, then the option applies only if the new code is
made subject to such option by the copyright holder.

Contributor(s):

Portions Copyrighted 2011 Gephi Consortium.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts.Data;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// YifanHu graph Layout in C#
    /// taken from gephi (java)
    /// License -- GPL v3
    /// </summary>
    public class YifanHuLayout : IGraphLayout
    {
        private YifanHuLayoutSettings settings;

        #region Getters and Setters
        public YifanHuLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public YifanHuLayout()
        {
            settings = new YifanHuLayoutSettings();
        }

        public YifanHuLayout(LayoutSettings settings)
        {
            if (settings is YifanHuLayoutSettings)
            {
                this.settings = (YifanHuLayoutSettings) settings;
            }
            else 
            {
                this.settings = new YifanHuLayoutSettings(settings);
            }
        }
        #endregion

        private AbstractForce GetEdgeForce() {
            return new SpringForce(settings.OptimalDistance);
        }

        private AbstractForce GetNodeForce() {
            return new ElectricalForce(settings.RelativeStrength, settings.OptimalDistance);
        }

        private void UpdateStep() {
            if (settings.AdaptiveCooling) {
                if (settings.Energy < settings.Energy0) {
                    settings.Progress++;
                    if (settings.Progress >= 5) {
                        settings.Progress = 0;
                        settings.Step = settings.Step / settings.StepRatio;
                    }
                } else {
                    settings.Progress = 0;
                    settings.Step = settings.Step * settings.StepRatio;
                }
            } else {
                settings.Step = settings.Step * settings.StepRatio;
            }
        }
    
        /// <summary>
        /// Sets graph to YifanHuLayout layout.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool  SetLayout(ref Graph graph)
        {
 	        settings.Energy = double.PositiveInfinity;
            settings.Progress = 0;
            settings.StepRatio = settings.InitialStep;
            Dictionary<Node, ForceVector> nodeData = new Dictionary<Node, ForceVector>();
            foreach (Node node in graph.Nodes) nodeData.Add(node, new ForceVector());

            for (int iteration = 0; iteration < settings.Iterations; iteration++)
            {

                // Evaluates n^2 inter node forces using BarnesHut.
                QuadTree tree = QuadTree.BuildTree(graph, settings.QuadTreeMaxLevel);
                BarnesHut barnes = new BarnesHut(GetNodeForce());
                barnes.Theta = settings.BarnesHutTheta;

                foreach (Node node in graph.Nodes) {
                    ForceVector data;
                    nodeData.TryGetValue(node, out data);
                    ForceVector f = barnes.CalculateForce(node, tree);
                    data.Add(f);
                }

                // Apply edge forces.
                foreach (Edge e in graph.Edges) {
                    if(e.Source != e.Target)
                    {
                        Node nf = graph.Nodes.Find(node => node.Id == e.Source);
                        Node nt = graph.Nodes.Find(node => node.Id == e.Target);

                        ForceVector f1, f2;
                        nodeData.TryGetValue(nf, out f1);
                        nodeData.TryGetValue(nt, out f2);

                        ForceVector f = GetEdgeForce().CalculateForce(nf, nt, ForceVectorUtils.Distance(nf, nt));
                        f1.Add(f);
                        f2.Subtract(f);
                    }
                }

                // Calculate energy and max force.
                settings.Energy0 = settings.Energy;
                settings.Energy = 0;
                double maxForce = 1;
                foreach (Node n in graph.Nodes) {
                    ForceVector data;
                    nodeData.TryGetValue(n, out data);
                    double norm = data.GetNorm();

                    settings.Energy += norm;
                    maxForce = Math.Max(maxForce, norm);
                }

                // Apply displacements on nodes.
                foreach (Node n in graph.Nodes) {
                    ForceVector data;
                    nodeData.TryGetValue(n, out data);
                    data.Multiply(1.0 / maxForce);
                    n.X += data.X;
                    n.Y += data.Y;
                }

                UpdateStep();
                if (Math.Abs((settings.Energy - settings.Energy0) / settings.Energy) < settings.ConvergenceThreshold) break;
            }

            if (settings.NoOverlap)
            {
                NoverlapLayout layout = new NoverlapLayout();
                layout.SetLayout(ref graph);
            }
            else
            {
                GraphUtils.UpdateBorders(ref graph, settings);
            }
            return true;
        }
}

    /// <summary>
    /// Fa = (n2 - n1) * ||n2 - n1|| / K
    /// @author Helder Suzuki <heldersuzuki@gephi.org>
    /// </summary>
    public class SpringForce : AbstractForce {

        public double OptimalDistance {get; set; }

        public SpringForce(double optimalDistance) {
            this.OptimalDistance = optimalDistance;
        }


        public override ForceVector CalculateForce(Node node1, Node node2, double distance)
        {
            ForceVector f = new ForceVector(node2.X - node1.X, node2.Y - node1.Y);
            f.Multiply(distance / OptimalDistance);
            return f;
        }

        public override ForceVector CalculateForce(Node node, QuadTree tree, double distance)
        {
            return null;
        }
    }

    /// <summary>
    /// Fr = -C*K*K*(n2-n1)/||n2-n1||
    /// @author Helder Suzuki <heldersuzuki@gephi.org>
    /// </summary>
    public class ElectricalForce : AbstractForce {

        public double RelativeStrength {get; set;}
        public double OptimalDistance {get; set;}

        public ElectricalForce(double relativeStrength, double optimalDistance) {
            this.RelativeStrength = relativeStrength;
            this.OptimalDistance = optimalDistance;
        }

        public override ForceVector CalculateForce(Node node1, Node node2, double distance) 
        {
            ForceVector f = new ForceVector(node2.X - node1.X, node2.Y - node1.Y);
            double scale = -RelativeStrength * OptimalDistance * OptimalDistance / (distance * distance);
            if (double.IsNaN(scale) || double.IsInfinity(scale))
            {
                scale = -1;
            }
            f.Multiply(scale);
            return f;
        }

        public override ForceVector CalculateForce(Node node, QuadTree tree, double distance)
        {
            return null;
        }
    }
}