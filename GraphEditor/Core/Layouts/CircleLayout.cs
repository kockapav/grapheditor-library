﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Layouts.Settings;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// Circle graph layout.
    /// 
    /// Sets node coordinates in graph for them to form a circle.
    /// </summary>
    public class CircleLayout : IGraphLayout
    {
        private CircleLayoutSettings settings;

        #region Getters and Setters
        public CircleLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CircleLayout()
        {
            settings = new CircleLayoutSettings();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public CircleLayout(LayoutSettings settings)
        {
            if (settings is CircleLayoutSettings)
            {
                this.settings = (CircleLayoutSettings) settings;
            }
            else 
            {
                this.settings = new CircleLayoutSettings(settings);
            }
        }
        #endregion

        /// <summary>
        /// Sets graph to CircleLayout.
        /// Sets node coordinates in graph for them to form a circle.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            int nodeCnt = graph.Nodes.Count;
            int index;
            double angle = -2 * Math.PI / nodeCnt;
            double radius = 1.5 * settings.NodeRadius * nodeCnt / Math.PI;

            
            index = 0;
            foreach (Node node in graph.Nodes)
            {
                node.X = radius * (Math.Cos((angle * index) + (Math.PI / 2))) + settings.OriginX;
                node.Y = radius * (Math.Sin((angle * index) + (Math.PI / 2))) + settings.OriginY;
                index++;
            }

            GraphUtils.UpdateBorders(ref graph, settings);
            graph.SetAttribute("nodeRadius", settings.NodeRadius.ToString());
            graph.SetAttribute("originX", settings.OriginX.ToString());
            graph.SetAttribute("originY", settings.OriginY.ToString());
            return true;
        }
    }
}
