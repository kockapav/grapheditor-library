﻿/*
Copyright 2008-2010 Gephi
Authors : Mathieu Jacomy
Website : http://www.gephi.org
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Layouts.Settings;
using GraphEditor.Core.Layouts.Data;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// This is a class implementing noverlap layout in C#,
    /// as it was implemented (in java) by Mathieu Jacomy in form of a plugin for gephi.
    /// License -- GPL v3
    /// </summary>
    public class NoverlapLayout : IGraphLayout
    {
        private NoverlapLayoutSettings settings;

        #region Getters and Setters
        public NoverlapLayoutSettings Settings
        {
            get { return settings; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public NoverlapLayout()
        {
            settings = new NoverlapLayoutSettings();
        }

        /// <summary>
        /// Constructor which takes all types of layout settings.
        /// </summary>
        /// <param name="settings">layout settings</param>
        public NoverlapLayout(LayoutSettings settings)
        {
            if (settings is NoverlapLayoutSettings)
            {
                this.settings = (NoverlapLayoutSettings) settings;
            }
            else 
            {
                this.settings = new NoverlapLayoutSettings(settings);
            }
        }
        #endregion

        /// <summary>
        /// Sets graph to NoverlapLayout.
        /// Tries to set node coordinates so no nodes will overlap.
        /// </summary>
        /// <param name="graph">graph, which layout will be changed</param>
        /// <returns>true when successful, else false</returns>
        public bool SetLayout(ref Graph graph)
        {
            // Init grid and add nodes to their boxes.
            SpatialGrid grid = new SpatialGrid(settings);
            foreach (Node node in graph.Nodes) grid.AddNode(node);

            // Creates node data set.
            Dictionary<Node, NodeLayoutData> nodeData = new Dictionary<Node, NodeLayoutData>();
            foreach (Node node in graph.Nodes) nodeData.Add(node, new NodeLayoutData());

            // Fills node data set with neighbours (build proximities).
            foreach (KeyValuePair<Tuple<int, int>, List<Node>> cell in grid.Data)
            {
                Tuple<int, int> tuple = cell.Key;
                List<Node> list = cell.Value;
                foreach (Node node in list)
                {
                    NodeLayoutData ndata;
                    nodeData.TryGetValue(node, out ndata);

                    foreach (KeyValuePair<Tuple<int, int>, List<Node>> cell2 in
                        grid.Data.Where(val => val.Key.Item1 >= tuple.Item1 - 1 &&
                                               val.Key.Item1 <= tuple.Item1 + 1 &&
                                               val.Key.Item2 >= tuple.Item2 - 1 &&
                                               val.Key.Item2 <= tuple.Item2 + 1))
                    {
                        Tuple<int, int> tuple2 = cell2.Key;
                        List<Node> list2 = cell2.Value;
                        foreach (Node node2 in list2)
                        {
                            if (node.Id != node2.Id)
                            {
                                ndata.Neighbours.Add(node2);
                            }
                        }
                    }
                }
            }

            // Apply repulsion force - along proximities...
            foreach (Node n1 in graph.Nodes)
            {
                NodeLayoutData n1data;
                nodeData.TryGetValue(n1, out n1data);

                foreach (Node n2 in n1data.Neighbours)
                {
                    double n1x = n1.X;
                    double n1y = n1.Y;
                    double n2x = n2.X;
                    double n2y = n2.Y;

                    string r1 = n1.GetAttributeValue("radius");
                    double n1radius = r1 == null ? settings.NodeRadius : double.Parse(r1);
                    string r2 = n2.GetAttributeValue("radius");
                    double n2radius = r2 == null ? settings.NodeRadius : double.Parse(r2);

                    // Check sizes (spheric)
                    double xDist = n2x - n1x;
                    double yDist = n2y - n1y;
                    double dist = Math.Sqrt(xDist * xDist + yDist * yDist);
                    bool collision = dist < (n1radius * settings.Ratio + settings.Margin) + (n2radius * settings.Ratio + settings.Margin);
                    if (collision) {
                        // n1 repulses n2, as strongly as it is big
                        NodeLayoutData n2data;
                        nodeData.TryGetValue(n2, out n2data);
                        double f = 1 + n1radius * 2;
                        if(dist>0){
                            n2data.DX += xDist / dist * f;
                            n2data.DY += yDist / dist * f;
                        } else {
                            // Same exact position, divide by zero impossible: jitter
                            Random rand = new Random();
                            n2data.DX += 0.01 * (0.5 - rand.NextDouble());
                            n2data.DY += 0.01 * (0.5 - rand.NextDouble());
                        }
                    }
                }
            }

            // apply forces
            foreach(Node node in graph.Nodes)
            {
                NodeLayoutData ndata;
                nodeData.TryGetValue(node, out ndata);
                ndata.DX *= 0.1 * settings.Speed;
                ndata.DY *= 0.1 * settings.Speed;
                node.X = node.X + ndata.DX;
                node.Y = node.Y + ndata.DY;
            }

            GraphUtils.UpdateBorders(ref graph, settings);

            return true;
        }

        #region SpatialGrid class
        /// <summary>
        /// Class used as a grid for dividing nodes into rows and columns.
        /// </summary>
        private class SpatialGrid
        {
            private int col_rows;
            private Dictionary<Tuple<int, int>, List<Node>> data;
            private NoverlapLayoutSettings settings;

            #region Getters and Setters
            public int Col_Rows
            {
                get { return col_rows; }
                set { col_rows = value; }
            }
            /// <summary>
            /// Spatial grid data.
            /// </summary>
            public Dictionary<Tuple<int, int>, List<Node>> Data
            {
                get { return data; }
            }
            #endregion

            #region Construcors
            /// <summary>
            /// Default construcor
            /// </summary>
            /// <param name="settings">noverlap layout settings</param>
            public SpatialGrid(NoverlapLayoutSettings settings)
            {
                this.col_rows = 20;
                this.data = new Dictionary<Tuple<int, int>, List<Node>>();
                this.settings = settings;
            }

            /// <summary>
            /// Constructor with different number of columns and rows used.
            /// </summary>
            /// <param name="col_rows">number of columns and rows</param>
            /// <param name="settings">noverlap layout settings</param>
            public SpatialGrid(int col_rows, NoverlapLayoutSettings settings)
            {
                this.col_rows = col_rows;
                this.data = new Dictionary<Tuple<int, int>, List<Node>>();
                this.settings = settings;
            }
            #endregion

            /// <summary>
            /// Adds node into grid
            /// </summary>
            /// <param name="n"></param>
            public void AddNode(Node node)
            {
                string rad = node.GetAttributeValue("radius");
                double radius = rad == null ? settings.NodeRadius : double.Parse(rad);

                // Get the rectangle occupied by the node
                double nxmin = node.X - (radius * settings.Ratio + settings.Margin);
                double nxmax = node.X + (radius * settings.Ratio + settings.Margin);
                double nymin = node.Y - (radius * settings.Ratio + settings.Margin);
                double nymax = node.Y + (radius * settings.Ratio + settings.Margin);

                // Get the rectangle as boxes
                int minXbox = (int)Math.Floor((col_rows - 1) * (nxmin - settings.MinX) / (settings.MaxX - settings.MinX));
                int maxXbox = (int)Math.Floor((col_rows - 1) * (nxmax - settings.MinX) / (settings.MaxX - settings.MinX));
                int minYbox = (int)Math.Floor((col_rows - 1) * (nymin - settings.MinY) / (settings.MaxY - settings.MinY));
                int maxYbox = (int)Math.Floor((col_rows - 1) * (nymax - settings.MinY) / (settings.MaxY - settings.MinY));
                for (int col = minXbox; col <= maxXbox; col++)
                {
                    for (int row = minYbox; row <= maxYbox; row++)
                    {
                        Tuple<int, int> t = new Tuple<int, int>(row, col);
                        List<Node> list;
                        data.TryGetValue(t, out list);
                        if (list == null)
                        {
                            list = new List<Node>();
                            data.Add(t, list);
                            list.Add(node);
                            
                        }
                        else
                        {
                            list.Add(node);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
