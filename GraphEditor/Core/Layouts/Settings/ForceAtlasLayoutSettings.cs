﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for ForceAtlasLayout.
    /// </summary>
    public class ForceAtlasLayoutSettings : LayoutSettings
    {
        public double inertia;
        private double repulsionStrength;
        private double attractionStrength;
        private double maxDisplacement;
        private bool freezeBalance;
        private double freezeStrength;
        private double freezeInertia;
        private double gravity;
        private double speed;
        private double cooling;
        private bool outboundAttractionDistribution;
        private bool adjustSizes;
        private double nodeRadius;
        private double edgeWeight;

        #region Getters And Setters
        public double EdgeWeight
        {
            get { return edgeWeight; }
            set { edgeWeight = value; }
        }

        public double NodeRadius
        {
            get { return nodeRadius; }
            set { nodeRadius = value; }
        }

        public double Inertia
        {
            get { return inertia; }
            set { inertia = value; }
        }

        public double RepulsionStrength
        {
            get { return repulsionStrength; }
            set { repulsionStrength = value; }
        }

        public double AttractionStrength
        {
            get { return attractionStrength; }
            set { attractionStrength = value; }
        }

        public double MaxDisplacement
        {
            get { return maxDisplacement; }
            set { maxDisplacement = value; }
        }

        public bool FreezeBalance
        {
            get { return freezeBalance; }
            set { freezeBalance = value; }
        }

        public double FreezeStrength
        {
            get { return freezeStrength; }
            set { freezeStrength = value; }
        }

        public double FreezeInertia
        {
            get { return freezeInertia; }
            set { freezeInertia = value; }
        }

        public double Gravity
        {
            get { return gravity; }
            set { gravity = value; }
        }

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public double Cooling
        {
            get { return cooling; }
            set { cooling = value; }
        }

        public bool OutboundAttractionDistribution
        {
            get { return outboundAttractionDistribution; }
            set { outboundAttractionDistribution = value; }
        }

        public bool AdjustSizes
        {
            get { return adjustSizes; }
            set { adjustSizes = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ForceAtlasLayoutSettings()
        {
            this.inertia = 0.1;
            this.repulsionStrength = 200;
            this.attractionStrength = 10;
            this.maxDisplacement = 10;
            this.freezeBalance = true;
            this.freezeStrength = 80;
            this.freezeInertia = 0.2;
            this.gravity = 30;
            this.outboundAttractionDistribution = false;
            this.adjustSizes = false;
            this.speed = 1;
            this.cooling = 1;
            this.nodeRadius = 10;
            this.edgeWeight = 1;
            this.Iterations = 50;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific ForceAtlasLayoutSettings values to default.
        /// </summary>
        /// <param name="s">layout settings with base values</param>
        public ForceAtlasLayoutSettings(LayoutSettings s)
            : base(s)
        {
            this.inertia = 0.1;
            this.repulsionStrength = 200;
            this.attractionStrength = 10;
            this.maxDisplacement = 10;
            this.freezeBalance = true;
            this.freezeStrength = 80;
            this.freezeInertia = 0.2;
            this.gravity = 30;
            this.outboundAttractionDistribution = false;
            this.adjustSizes = false;
            this.speed = 1;
            this.cooling = 1;
            this.nodeRadius = 10;
            this.edgeWeight = 1;
            this.Iterations = 50;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="s">layout settings to copy from</param>
        public ForceAtlasLayoutSettings(ForceAtlasLayoutSettings s) : base(s)
        {
            this.inertia = s.Inertia;
            this.repulsionStrength = s.RepulsionStrength;
            this.attractionStrength = s.AttractionStrength;
            this.maxDisplacement = s.MaxDisplacement;
            this.freezeBalance = s.FreezeBalance;
            this.freezeStrength = s.FreezeStrength;
            this.freezeInertia = s.FreezeInertia;
            this.gravity = s.Gravity;
            this.outboundAttractionDistribution = s.OutboundAttractionDistribution;
            this.adjustSizes = s.AdjustSizes;
            this.speed = s.Speed;
            this.cooling = s.Cooling;
            this.nodeRadius = s.NodeRadius;
            this.edgeWeight = s.edgeWeight;
        }

        /// <summary>
        /// ForceAtlasLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public ForceAtlasLayoutSettings(Graph graph)
            : base(graph)
        {
            this.inertia = 0.1;
            this.repulsionStrength = 200;
            this.attractionStrength = 10;
            this.maxDisplacement = 10;
            this.freezeBalance = true;
            this.freezeStrength = 80;
            this.freezeInertia = 0.2;
            this.gravity = 30;
            this.outboundAttractionDistribution = false;
            this.adjustSizes = false;
            this.speed = 1;
            this.cooling = 1;
            this.nodeRadius = 10;
            this.edgeWeight = 1;
            this.Iterations = 50;
        }
        #endregion
    }
}
