﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class containing default layout settings.
    /// Can be extended by specific layout for its own settings.
    /// </summary>
    public class LayoutSettings
    {
        private double minX;
        private double minY;
        private double maxX;
        private double maxY;
        private double gap;
        private bool forceBorders;
        private int iterations;
        private bool noverlap;

        #region Getters and Setters
        /// <summary>
        /// Number of iterations.
        /// Some layout algorithms are running several times for them to take effect.
        /// This parameter defines how many times should algorithm run in loop.
        /// </summary>
        public int Iterations
        {
            get { return iterations; }
            set { iterations = value; }
        }

        public double MinX
        {
            get { return minX; }
            set { minX = value; }
        }

        public double MinY
        {
            get { return minY; }
            set { minY = value; }
        }

        public double MaxX
        {
            get { return maxX; }
            set { maxX = value; }
        }

        public double MaxY
        {
            get { return maxY; }
            set { maxY = value; }
        }

        /// <summary>
        /// Defines the gap in graph borders to node minX, maxX, minY, maxY values.
        /// Used only, when ForceLayout is set false.
        /// </summary>
        public double Gap
        {
            get { return gap; }
            set { gap = value; }
        }

        /// <summary>
        /// When set true, borders of graph are set to values specified by minX, maxX, minY, maxY,
        /// When set false, borders of graph are calculated from node positions in graph.
        /// </summary>
        public bool ForceBorders
        {
            get { return forceBorders; }
            set { forceBorders = value; }
        }

        /// <summary>
        /// If true, some layout algorithms call set graph to NoOverlapLayout,
        /// so no nodes in graph will overlap.
        /// </summary>
        public bool NoOverlap
        {
            get { return noverlap; }
            set { noverlap = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default LayoutSettings constructor.
        /// Sets all values to default.
        /// </summary>
        public LayoutSettings()
        {
            this.minX = 0;
            this.minY = 0;
            this.maxX = 1024;
            this.maxY = 768;
            this.gap = 10;
            this.iterations = 1;
            this.forceBorders = false;
            this.noverlap = true;
        }

        /// <summary>
        /// LayoutSettings copy constructor.
        /// </summary>
        /// <param name="settings">layout settings to copy values from</param>
        public LayoutSettings(LayoutSettings settings)
        {
            this.minX = settings.MinX;
            this.minY = settings.MinY;
            this.maxX = settings.MaxX;
            this.maxY = settings.MaxY;
            this.gap = settings.Gap;
            this.iterations = settings.Iterations;
            this.forceBorders = settings.ForceBorders;
            this.noverlap = settings.NoOverlap;
        }

        /// <summary>
        /// LayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public LayoutSettings(Graph graph)
        {
            string minX = graph.GetAttributeValue("minX");
            string minY = graph.GetAttributeValue("minY");
            string maxX = graph.GetAttributeValue("maxX");
            string maxY = graph.GetAttributeValue("maxY");
            this.gap = 10;

            this.minX = (minX == null) ? 0 : double.Parse(minX);
            this.minY = (minY == null) ? 0 : double.Parse(minY);
            this.maxX = (maxX == null) ? 0 : double.Parse(maxX);
            this.maxY = (maxY == null) ? 0 : double.Parse(maxY);
            this.iterations = 1;
            this.forceBorders = false;
            this.noverlap = true;
        }
        #endregion
    }
}
