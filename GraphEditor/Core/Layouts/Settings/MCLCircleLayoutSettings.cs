﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for MCLCircleLayout.
    /// </summary>
    public class MCLCircleLayoutSettings : LayoutSettings
    {
        private double nodeRadius;
        private double originX;
        private double originY;
        private int expansionsCnt;
        private int inflationsCnt;

        #region Getters and Setters
        /// <summary>
        /// Number of expansions done in one cycle of mcl.
        /// </summary>
        public int ExpansionsCnt
        {
            get { return expansionsCnt; }
            set { expansionsCnt = value; }
        }

        /// <summary>
        /// Number of inflations done in one cycle of mcl.
        /// </summary>
        public int InflationsCnt
        {
            get { return inflationsCnt; }
            set { inflationsCnt = value; }
        }

        /// <summary>
        /// Default node radius value, used to determine size of nodes in graph.
        /// </summary>
        public double NodeRadius
        {
            get { return nodeRadius; }
            set { nodeRadius = value; }
        }

        /// <summary>
        /// origin of the circle X coordinate
        /// </summary>
        public double OriginX
        {
            get { return originX; }
            set { originX = value; }
        }

        /// <summary>
        /// origin of the circle Y coordinate
        /// </summary>
        public double OriginY
        {
            get { return originY; }
            set { originY = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default CircleLayoutSettings constructor.
        /// Sets all values to default.
        /// </summary>
        public MCLCircleLayoutSettings()
        {
            this.nodeRadius = 10;
            this.originX = 0;
            this.originY = 0;
            this.expansionsCnt = 2;
            this.inflationsCnt = 4;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific CircleLayoutSettings values to default.
        /// </summary>
        /// <param name="s">layout settings with base values</param>
        public MCLCircleLayoutSettings(LayoutSettings s) : base(s)
        {
            this.nodeRadius = 10;
            this.originX = 0;
            this.originY = 0;
            this.expansionsCnt = 2;
            this.inflationsCnt = 4;
        }

        /// <summary>
        /// CircleLayoutSettings copy constructor.
        /// </summary>
        /// <param name="s">layout settings to copy from</param>
        public MCLCircleLayoutSettings(MCLCircleLayoutSettings s)
            : base(s)
        {
            this.nodeRadius = s.NodeRadius;
            this.originX = s.OriginX;
            this.originY = s.OriginY;
            this.expansionsCnt = s.ExpansionsCnt;
            this.inflationsCnt = s.InflationsCnt;
        }

        /// <summary>
        /// CircleLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public MCLCircleLayoutSettings(Graph graph)
            : base(graph) 
        {
            string nodeRadius = graph.GetAttributeValue("nodeRadius");
            string originX = graph.GetAttributeValue("originX");
            string originY = graph.GetAttributeValue("originY");

            this.nodeRadius = (nodeRadius == null) ? 10 : double.Parse(nodeRadius);
            this.originX = (originX == null) ? 0 : double.Parse(originX);
            this.originY = (originY == null) ? 0 : double.Parse(originY);
            this.expansionsCnt = 2;
            this.inflationsCnt = 4;
        }
        #endregion
    }
}
