﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for ScaleLayout.
    /// </summary>
    public class ScaleLayoutSettings : LayoutSettings
    {
        private double scale;

        #region Getters and Setters
        /// <summary>
        /// Scale value used for scaling graph, 
        /// Scale more then 1 expands graph, Scale less then 1 contracts graph.
        /// </summary>
        public double Scale
        {
            get { return scale; }
            set { scale = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default ScaleLayoutSettings constructor.
        /// Sets all values to default.
        /// </summary>
        public ScaleLayoutSettings()
        {
            this.scale = 1.5;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific RotateLayoutSettings values to default.
        /// </summary>
        /// <param name="s"></param>
        public ScaleLayoutSettings(LayoutSettings s) : base(s)
        {
            this.scale = 1.5;
        }

        /// <summary>
        /// RotateLayoutSettings copy constructor.
        /// </summary>
        /// <param name="s">layout settings to copy from</param>
        public ScaleLayoutSettings(ScaleLayoutSettings s)
            : base(s)
        {
            this.scale = s.Scale;
        }

        /// <summary>
        /// CircleLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public ScaleLayoutSettings(Graph graph) : base(graph) 
        {
            this.scale = 1.5;
        }
        #endregion
    }
}
