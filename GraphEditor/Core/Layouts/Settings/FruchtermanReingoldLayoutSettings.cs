﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for FruchtermanReingoldLayout.
    /// </summary>
    public class FruchtermanReingoldLayoutSettings : LayoutSettings
    {
        private double area;
        private double gravity;
        private double speed;

        #region Getters and Setters
        public double Area
        {
            get { return area; }
            set { area = value; }
        }

        public double Gravity
        {
            get { return gravity; }
            set { gravity = value; }
        }

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public FruchtermanReingoldLayoutSettings()
        {
            this.speed = 50;
            this.area = 10000;
            this.gravity = 100;
            this.Iterations = 100;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific FruchtermanReingoldLayoutSettings values to default.
        /// </summary>
        /// <param name="s">layout settings with base values</param>
        public FruchtermanReingoldLayoutSettings(LayoutSettings s)
            : base(s)
        {
            this.speed = 50;
            this.area = 10000;
            this.gravity = 100;
            this.Iterations = 100;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="s">layout settings to copy from</param>
        public FruchtermanReingoldLayoutSettings(FruchtermanReingoldLayoutSettings s) : base(s)
        {
            this.speed = s.Speed;
            this.area = s.Area;
            this.gravity = s.Gravity;
        }

        /// <summary>
        /// FruchtermanReingoldLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public FruchtermanReingoldLayoutSettings(Graph graph)
            : base(graph)
        {
            this.speed = 50;
            this.area = 10000;
            this.gravity = 100;
            this.Iterations = 100;
        }
        #endregion
    }
}
