﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for NoverlapLayout.
    /// </summary>
    public class NoverlapLayoutSettings : LayoutSettings
    {
        private double speed;
        private double ratio;
        private double margin;
        private double nodeRadius;

        #region Getters and Setters
        /// <summary>
        /// Default node radius value, used to determine size of nodes in graph.
        /// </summary>
        public double NodeRadius
        {
            get { return nodeRadius; }
            set { nodeRadius = value; }
        }

        /// <summary>
        /// Speed
        /// </summary>
        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        /// <summary>
        /// Ratio
        /// </summary>
        public double Ratio
        {
            get { return ratio; }
            set { ratio = value; }
        }

        /// <summary>
        /// Margin
        /// </summary>
        public double Margin
        {
            get { return margin; }
            set { margin = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public NoverlapLayoutSettings()
        {
            this.speed = 3;
            this.ratio = 1.2;
            this.margin = 5;
            this.nodeRadius = 10;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific NoverlapLayoutSettings values to default.
        /// </summary>
        /// <param name="s">layout settings with base values</param>
        public NoverlapLayoutSettings(LayoutSettings s)
            : base(s)
        {
            this.speed = 3;
            this.ratio = 1.2;
            this.margin = 5;
            this.nodeRadius = 10;
        }

        /// <summary>
        /// NoverlapLayoutSettings copy constructor
        /// </summary>
        /// <param name="s">layout settings to copy from</param>
        public NoverlapLayoutSettings(NoverlapLayoutSettings s) : base(s)
        {
            this.speed = s.Speed;
            this.ratio = s.Ratio;
            this.margin = s.Margin;
            this.nodeRadius = s.NodeRadius;
        }

        /// <summary>
        /// NoverlapLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public NoverlapLayoutSettings(Graph graph)
            : base(graph)
        {
            string nodeRadius = graph.GetAttributeValue("nodeRadius");
            this.nodeRadius = (nodeRadius == null) ? 10 : double.Parse(nodeRadius);
            this.speed = 3;
            this.ratio = 1.2;
            this.margin = 5;
        }
        #endregion
    }
}
