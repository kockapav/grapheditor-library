﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for RotateLayout.
    /// </summary>
    public class RotateLayoutSettings : LayoutSettings
    {
        private double angle;
        private bool clockwise;
        private double originX;
        private double originY;

        #region Getters and Setters
        /// <summary>
        /// Angle of rotation in degrees.
        /// </summary>
        public double Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        /// <summary>
        /// Bool value deciding if rotation is going to be made clockwise (true) or counter-clockwise (false).
        /// </summary>
        public bool Clockwise
        {
            get { return clockwise; }
            set { clockwise = value; }
        }

        /// <summary>
        /// origin X coordinate of rotation
        /// </summary>
        public double OriginX
        {
            get { return originX; }
            set { originX = value; }
        }

        /// <summary>
        /// origin Y coordinate of rotation
        /// </summary>
        public double OriginY
        {
            get { return originY; }
            set { originY = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default RotateLayoutSettings constructor.
        /// Sets all values to default.
        /// </summary>
        public RotateLayoutSettings()
        {
            this.angle = 90;
            this.clockwise = true;
            this.originX = 0;
            this.originY = 0;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific RotateLayoutSettings values to default.
        /// </summary>
        /// <param name="s"></param>
        public RotateLayoutSettings(LayoutSettings s) : base(s)
        {
            this.angle = 90;
            this.clockwise = true;
            this.originX = 0;
            this.originY = 0;
        }

        /// <summary>
        /// RotateLayoutSettings copy constructor.
        /// </summary>
        /// <param name="s">layout settings to copy from</param>
        public RotateLayoutSettings(RotateLayoutSettings s)
            : base(s)
        {
            this.angle = s.Angle;
            this.clockwise = s.Clockwise;
            this.originX = s.OriginX;
            this.originY = s.OriginY;
        }

        /// <summary>
        /// CircleLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public RotateLayoutSettings(Graph graph) : base(graph) 
        {
            this.angle = 90;
            this.clockwise = true;
            string originX = graph.GetAttributeValue("originX");
            string originY = graph.GetAttributeValue("originY");
            this.originX = (originX == null) ? 0 : double.Parse(originX);
            this.originY = (originY == null) ? 0 : double.Parse(originY);
        }
        #endregion
    }
}
