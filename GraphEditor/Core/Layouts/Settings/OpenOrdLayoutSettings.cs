﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for YifanHuLayout.
    /// </summary>
    public class OpenOrdLayoutSettings : LayoutSettings
    {
        private double edgeCut;
        private double realTime;

        #region Getters and Setters
        public double EdgeCut
        {
            get { return edgeCut; }
            set { edgeCut = value; }
        }

        public double RealTime
        {
            get { return realTime; }
            set { realTime = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public OpenOrdLayoutSettings()
        {
            this.edgeCut = 0.5;
            this.realTime = 0.5;
            Iterations = 800;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific OpenOrdLayoutSettings values to default.
        /// </summary>
        /// <param name="s">layout settings with base values</param>
        public OpenOrdLayoutSettings(LayoutSettings s)
            : base(s)
        {
            this.edgeCut = 0.5;
            this.realTime = 0.5;
            Iterations = s.Iterations;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="s"></param>
        public OpenOrdLayoutSettings(OpenOrdLayoutSettings s) : base(s)
        {
            this.edgeCut = s.EdgeCut;
            this.realTime = s.RealTime;
            Iterations = s.Iterations;
        }

        /// <summary>
        /// OpenOrdLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public OpenOrdLayoutSettings(Graph g)
            : base(g)
        {
            this.edgeCut = 0.5;
            this.realTime = 0.5;
            Iterations = 800;
        }
        #endregion
    }
}
