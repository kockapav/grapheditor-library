﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts.Settings
{
    /// <summary>
    /// Class extending default layout settings with custom settings for YifanHuLayout.
    /// </summary>
    public class YifanHuLayoutSettings : LayoutSettings
    {
        public double OptimalDistance { get; set; }
        public double RelativeStrength { get; set; }
        public double Step { get; set; }
        public double InitialStep { get; set; }
        public int Progress { get; set; }
        public double StepRatio { get; set; }
        public int QuadTreeMaxLevel { get; set; }
        public double BarnesHutTheta { get; set; }
        public double ConvergenceThreshold { get; set; }
        public bool AdaptiveCooling { get; set; }
        public double Energy0 { get; set; }
        public double Energy { get; set; }

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public YifanHuLayoutSettings()
        {
            StepRatio = 0.95;
            RelativeStrength = 0.2;
            OptimalDistance = 700;
            InitialStep = OptimalDistance / 5;
            Step = InitialStep;
            QuadTreeMaxLevel = 10;
            BarnesHutTheta = 1.0;
            AdaptiveCooling = true;
            ConvergenceThreshold = 1e-4;
            Iterations = 500;
        }

        /// <summary>
        /// Constructor which copies LayoutSettings values,
        /// but sets specific YifanHuLayoutSettings values to default.
        /// </summary>
        /// <param name="s">layout settings with base values</param>
        public YifanHuLayoutSettings(LayoutSettings s)
            : base(s)
        {
            StepRatio = 0.95;
            RelativeStrength = 0.2;
            OptimalDistance = 700;
            InitialStep = OptimalDistance / 5;
            Step = InitialStep;
            QuadTreeMaxLevel = 10;
            BarnesHutTheta = 1.0;
            AdaptiveCooling = true;
            ConvergenceThreshold = 1e-4;
            Iterations = s.Iterations;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="s"></param>
        public YifanHuLayoutSettings(YifanHuLayoutSettings s) : base(s)
        {
            this.StepRatio = s.StepRatio;
            this.RelativeStrength = s.RelativeStrength;
            this.OptimalDistance = s.OptimalDistance;
            this.InitialStep = s.InitialStep;
            this.Step = s.Step;
            this.QuadTreeMaxLevel = s.QuadTreeMaxLevel;
            this.BarnesHutTheta = s.BarnesHutTheta;
            this.AdaptiveCooling = s.AdaptiveCooling;
            this.ConvergenceThreshold = s.ConvergenceThreshold;
            Iterations = s.Iterations;
        }

        /// <summary>
        /// YifanHuLayoutSettings which takes a loaded graph as a parameter.
        /// Tries to load settings from graph attributes.
        /// </summary>
        /// <param name="graph">loaded graph</param>
        public YifanHuLayoutSettings(Graph g)
            : base(g)
        {
            StepRatio = 0.95;
            RelativeStrength = 0.2;
            OptimalDistance = 700;
            InitialStep = OptimalDistance / 5;
            Step = InitialStep;
            QuadTreeMaxLevel = 10;
            BarnesHutTheta = 1.2;
            AdaptiveCooling = true;
            ConvergenceThreshold = 1e-4;
            Iterations = 500;
        }
        #endregion
    }
}
