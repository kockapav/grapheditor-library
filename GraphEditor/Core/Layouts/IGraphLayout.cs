﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// Interface used for graph Layouts.
    /// </summary>
    public interface IGraphLayout
    {
        /// <summary>
        /// Changes graph layout.
        /// </summary>
        /// <param name="graph">graph, which layout should be changed</param>
        /// <returns>true when layout is changed successfully, else false</returns>
        bool SetLayout(ref Graph graph);
    }
}
