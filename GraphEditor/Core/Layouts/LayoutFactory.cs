﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;

namespace GraphEditor.Core.Layouts
{
    /// <summary>
    /// LayoutFactory class,
    /// initializes implementation of IGraphLayout based on chosen format id.
    /// </summary>
    public class LayoutFactory
    {
        /// <summary>
        /// Returns graph layout based on given format id (LayoutsList.xml)
        /// </summary>
        /// <param name="id">id of layout</param>
        /// <returns>chosen graph layout</returns>
        public IGraphLayout GetLayout(int id)
        {
            switch (id)
            {
                case 9: return new MCLCircleLayout();
                case 8: return new OpenOrdLayout();
                case 7: return new YifanHuLayout();
                case 6: return new ForceAtlasLayout();
                case 5: return new FruchtermanReingoldLayout();
                case 4: return new NoverlapLayout();
                case 3: return new ScaleLayout();
                case 2: return new RotateLayout();
                case 1: return new CircleLayout();
                case 0:
                default: return new RandomLayout();
            }
        }

        /// <summary>
        /// Returns graph layout with given settings based on given format id (LayoutsList.xml)
        /// </summary>
        /// <param name="id">id of layout</param>
        /// <param name="settings">layout settings</param>
        /// <returns>chosen graph layout with applied settings</returns>
        public IGraphLayout GetLayout(int id, LayoutSettings settings)
        {
            switch (id)
            {
                case 9: return new MCLCircleLayout(settings);
                case 8: return new OpenOrdLayout(settings);
                case 7: return new YifanHuLayout(settings);
                case 6: return new ForceAtlasLayout(settings);
                case 5: return new FruchtermanReingoldLayout(settings);
                case 4: return new NoverlapLayout(settings);
                case 3: return new ScaleLayout(settings);
                case 2: return new RotateLayout(settings);
                case 1: return new CircleLayout(settings);
                case 0:
                default: return new RandomLayout(settings);
            }
        }
    }
}
