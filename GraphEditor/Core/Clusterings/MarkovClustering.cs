﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Clusterings
{
    /// <summary>
    /// The Markov Cluster Algorithm (MCL) class
    /// </summary>
    public class MarkovClustering
    {
        private const double delta = 1.0E-15;
        private double[,] adjMatrix;
        private int nodeCnt;

        /// <summary>
        /// Performs MCL on given undirected graph.
        /// Saves number of clusters in graph as attribute.
        /// Saves cluster number to each node.
        /// </summary>
        /// <param name="graph">undirected graph to be clusterized</param>
        /// <param name="expCnt">number of expansions in one cycle</param>
        /// <param name="infCnt">number of inflations in one cycle</param>
        /// <returns>true when succesfull, else false</returns>
        public bool ClusterizeGraph(ref Graph graph, int expCnt, int infCnt)
        {
            nodeCnt = graph.Nodes.Count;
            for (int i = 0; i < nodeCnt; i++)
            {
                graph.Nodes[i].SetAttribute("clIndex", i.ToString());
            }

            InitAdjacencyMatrix(graph);
            NormalizeAdjacencyMatrix();

            for (int i = 0; i < 100; i++)
            {
                ExpandAdjMatrix(expCnt);
                InflateAdjMatrix(infCnt);
                if (IsConvergent()) break;
            }

            InterceptClusters(ref graph);

            for (int i = 0; i < nodeCnt; i++)
            {
                graph.Nodes[i].RemoveAttribute("clIndex");
            }
            return true;
        }

        /// <summary>
        /// Intercepts clusters based on results in adjacent matrix
        /// </summary>
        /// <param name="graph">used graph</param>
        private void InterceptClusters(ref Graph graph)
        {
            int[] cluster = new int[nodeCnt];
            for (int i = 0; i < nodeCnt; i++) cluster[i] = -1;

            for (int row = 0; row < nodeCnt; row++)
            {
                for (int col = 0; col < nodeCnt; col++)
                {
                    if (adjMatrix[row, col] > 0)
                    {
                        if (cluster[row] == -1)
                        {
                            cluster[row] = row;
                            cluster[col] = row;
                        }
                        else
                        {
                            cluster[col] = cluster[row];
                        }
                    }
                }
            }

            Dictionary<int, int> clSet = new Dictionary<int,int>();
            for (int i = 0; i < nodeCnt; i++)
            {
                if(!clSet.Keys.Contains(cluster[i])){
                    clSet.Add(cluster[i], clSet.Count);
                }
            }

            graph.SetAttribute("clusters", clSet.Count.ToString());

            foreach (Node n in graph.Nodes)
            {
                int index = int.Parse(n.GetAttributeValue("clIndex"));
                n.SetAttribute("cluster", clSet[cluster[index]].ToString());
            }
        }

        /// <summary>
        /// Returns true if adjacency matrix has reached convergent state
        /// </summary>
        /// <returns>true if convergent, else false</returns>
        private bool IsConvergent()
        {
            for (int col = 0; col < nodeCnt; col++)
            {
                bool hasValue = false;
                double value = 0;
                for (int i = 0; i < nodeCnt; i++)
                {
                    if (!hasValue)
                    {
                        if (adjMatrix[i, col] > 0)
                        {
                            hasValue = true;
                            value = adjMatrix[i, col];
                        }
                    }
                    else
                    {
                        if (adjMatrix[i, col] > 0 && (adjMatrix[i, col] < value - delta || adjMatrix[i, col] > value + delta)) return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Expands adjacency matrix expCnt times.
        /// </summary>
        /// <param name="expCnt">cnt of expansions</param>
        private void ExpandAdjMatrix(int expCnt)
        {
            for (int exp = 0; exp < expCnt; exp++)
            {
                double[,] newMatrix = new double[nodeCnt, nodeCnt];
                for (int row = 0; row < nodeCnt; row++)
                {
                    for (int col = 0; col < nodeCnt; col++)
                    {
                        newMatrix[row, col] = 0;
                        for (int k = 0; k < nodeCnt; k++)
                        {
                            newMatrix[row, col] += adjMatrix[row, k] * adjMatrix[k, col];
                        }
                    }
                }
                adjMatrix = newMatrix;
            }
        }

        /// <summary>
        /// Inflates adjacency matrix infCnt times
        /// </summary>
        /// <param name="infCnt">cnt of inflations</param>
        private void InflateAdjMatrix(int infCnt)
        {
            for (int col = 0; col < nodeCnt; col++)
            {
                double sum = 0;
                for (int row = 0; row < nodeCnt; row++)
                {
                    adjMatrix[row, col] = Math.Pow(adjMatrix[row, col], infCnt);
                    sum += adjMatrix[row, col];
                }

                for (int row = 0; row < nodeCnt; row++)
                {
                    adjMatrix[row, col] = adjMatrix[row, col] / sum;
                }
            }
        }

        /// <summary>
        /// Initializes adjacency matrix from graph.
        /// </summary>
        /// <param name="graph">used graph</param>
        private void InitAdjacencyMatrix(Graph graph)
        {
            adjMatrix = new double[nodeCnt, nodeCnt];
            for (int i = 0; i < nodeCnt; i++)
            {
                for (int j = 0; j < nodeCnt; j++)
                {
                    if (i == j)
                    {
                        adjMatrix[i, j] = 1;
                    }
                    else
            
                    {
                        adjMatrix[i, j] = 0;
                    }
                }
            }

            foreach (Edge e in graph.Edges)
            {
                string w = e.GetAttributeValue("weight");
                double weight = w == null ? 1 : double.Parse(w);
                Node n1 = graph.Nodes.Find(node => node.Id == e.Source);
                Node n2 = graph.Nodes.Find(node => node.Id == e.Target);
                adjMatrix[int.Parse(n1.GetAttributeValue("clIndex")), int.Parse(n2.GetAttributeValue("clIndex"))] = weight;
                adjMatrix[int.Parse(n2.GetAttributeValue("clIndex")), int.Parse(n1.GetAttributeValue("clIndex"))] = weight;
            }
        }

        /// <summary>
        /// Normalizes the adjacency matrix
        /// </summary>
        private void NormalizeAdjacencyMatrix()
        {
            for (int col = 0; col < nodeCnt; col++)
            {
                double sum = 0;
                for (int row = 0; row < nodeCnt; row++)
                {
                    sum += adjMatrix[row, col];
                }

                for (int row = 0; row < nodeCnt; row++)
                {
                    adjMatrix[row, col] = adjMatrix[row, col] / sum;
                }
            }
        }
    }
}
