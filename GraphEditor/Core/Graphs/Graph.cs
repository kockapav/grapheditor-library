﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphEditor.Core.Graphs
{
    /// <summary>
    /// Class representing a the Graph.
    /// Has Nodes, Edges and a Dictionary of optional Attributes.
    /// </summary>
    public class Graph
    {
        private int id;
        private List<Node> nodes;
        private List<Edge> edges;
        private Dictionary<string, string> attributes;

        #region Getters and Setters
        /// <summary>
        /// Returns graph id.
        /// </summary>
        public int Id 
        { 
            get { return id; }
        }

        /// <summary>
        /// Returns list of all graph nodes.
        /// </summary>
        public List<Node> Nodes 
        { 
            get { return nodes; }
        }

        /// <summary>
        /// Returns list of all graph edges.
        /// </summary>
        public List<Edge> Edges 
        { 
            get { return edges; }
        }

        /// <summary>
        /// Returns dictionary with all attributes.
        /// </summary>
        public Dictionary<string, string> Attributes
        {
            get { return attributes; }
        }

        /// <summary>
        /// Attribute value getter
        /// </summary>
        /// <param name="key">key (name) of attribute in string</param>
        /// <returns>value of attribute, null if key value is not found</returns>
        public string GetAttributeValue(string key)
        {
            string value;
            attributes.TryGetValue(key, out value);
            return value;
        }

        /// <summary>
        /// Sets attribute value.
        /// Creates a new attribute if key value does not exist.
        /// </summary>
        /// <param name="key">key (name) of attribute in string</param>
        /// <param name="value">value of attribute in string</param>
        public void SetAttribute(string key, string value)
        {
            attributes[key] = value;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default graph constructor
        /// </summary>
        /// <param name="id">id of new graph</param>
        public Graph(int id)
        {
            this.id = id;
            this.nodes = new List<Node>();
            this.edges = new List<Edge>();
            this.attributes = new Dictionary<string, string>();
        }
        #endregion

        /// <summary>
        /// Adds node in nodes list
        /// </summary>
        /// <param name="n">node to be added</param>
        /// <returns>true if node has been added to graph, else false.</returns>
        public bool AddNode(Node n)
        {
            if (nodes.Find(node => node.Id == n.Id) != null) return false;
            nodes.Add(n);
            return true;
        }

        /// <summary>
        /// Removes node specified by id from nodes list.
        /// Removes all edges which have this node id set as source or target.
        /// Then removes these edges from other nodes.
        /// </summary>
        /// <param name="id">id of node to be removed</param>
        /// <returns>true if node is removed from graph, else false.</returns>
        public bool RemoveNode(int id)
        {
            Node n = nodes.Find(node => node.Id == id);
            if (n == null) return false;
            List<Edge> nodeEdges = new List<Edge>();
            foreach (Edge e in n.Edges) nodeEdges.Add(e);
            foreach (Edge e in nodeEdges) RemoveEdge(e.Source, e.Target);
            nodes.Remove(n);
            return true;
        }

        /// <summary>
        /// Adds edge in edges list.
        /// Adds edge to source and target nodes.
        /// </summary>
        /// <param name="e">edge to be added</param>
        /// <returns>true if edge has been added to graph, else false.</returns>
        public bool AddEdge(Edge e)
        {
            Node source, target;
            source = nodes.Find(node => node.Id == e.Source);
            target = nodes.Find(node => node.Id == e.Target);
            if (source == null || target == null) return false;
            edges.Add(e);
            source.AddEdge(e);
            target.AddEdge(e);
            return true;
        }

        /// <summary>
        /// Removes edge specified by node source and target id from edges list.
        /// Removes edge from source and target nodes.
        /// </summary>
        /// <param name="id">id of removed edge</param>
        /// <returns>true if edge is removed from graph, else false.</returns>
        public bool RemoveEdge(int id)
        {
            Edge e = edges.Find(node => node.Id == id);
            return RemoveEdge(e);
        }

        /// <summary>
        /// Removes edge specified by node source and target id from edges list.
        /// Removes edge from source and target nodes.
        /// </summary>
        /// <param name="source">source node id</param>
        /// <param name="target">target node id</param>
        /// <returns>true if edge is removed from graph, else false.</returns>
        public bool RemoveEdge(int source, int target)
        {
            Edge e = edges.Find(node => node.Source == source && node.Target == target);
            return RemoveEdge(e);
        }

        /// <summary>
        /// Removes edge from graph.
        /// Removes edge from source and target nodes.
        /// </summary>
        /// <param name="e">edge to be removed</param>
        /// <returns>true if edge is removed from graph, else false.</returns>
        private bool RemoveEdge(Edge e)
        {
            Node snode, tnode;
            snode = nodes.Find(node => node.Id == e.Source);
            tnode = nodes.Find(node => node.Id == e.Target);
            if (e == null || snode == null || tnode == null) return false;
            edges.Remove(e);
            snode.RemoveEdge(e);
            tnode.RemoveEdge(e);
            return true;
        }
    }
}
