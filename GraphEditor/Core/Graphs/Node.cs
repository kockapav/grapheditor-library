﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphEditor.Core.Graphs
{
    /// <summary>
    /// Class representing a Node in Graph.
    /// Has a List of Edges and Dictionary of optional attributes.
    /// </summary>
    public class Node
    {
        private double x, y;
        private int id;
        private List<Edge> edges;
        private Dictionary<string, string> attributes;

        #region Getters and Setters
        /// <summary>
        /// Node x coordinate.
        /// </summary>
        public double X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        /// <summary>
        /// Node y coordinate.
        /// </summary>
        public double Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <summary>
        /// Returns node id.
        /// </summary>
        public int Id
        { 
            get { return id; }
        }

        /// <summary>
        /// Returns dictionary with all attributes.
        /// </summary>
        public Dictionary<string, string> Attributes
        {
            get { return attributes; }
        }

        /// <summary>
        /// Returns a list of all edges which have this node set as a source or target.
        /// </summary>
        public List<Edge> Edges { get { return edges; }}

        /// <summary>
        /// Attribute value getter
        /// </summary>
        /// <param name="key">key (name) of attribute in string</param>
        /// <returns>value of attribute, null if key value is not found</returns>
        public string GetAttributeValue(string key)
        {
            string value;
            attributes.TryGetValue(key, out value);
            return value;
        }

        /// <summary>
        /// Sets attribute value.
        /// Creates a new attribute if key value does not exist.
        /// </summary>
        /// <param name="key">key (name) of attribute in string</param>
        /// <param name="value">value of attribute in string</param>
        public void SetAttribute(string key, string value)
        {
            attributes[key] = value;
        }

        /// <summary>
        /// Removes an attribute
        /// </summary>
        /// <param name="key">attribute name</param>
        public void RemoveAttribute(string key)
        {
            attributes.Remove(key);
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor for Node
        /// </summary>
        /// <param name="id">id of new node</param>
        public Node(int id)
        {
            this.x = 0;
            this.y = 0;
            this.id = id;
            this.edges = new List<Edge>();
            this.attributes = new Dictionary<string, string>();
        }

        /// <summary>
        /// Overloaded constructor for Node
        /// </summary>
        /// <param name="id">id of new node</param>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        /// <param name="attributes">dictionary of optional attributes</param>
        public Node(int id, double x, double y, Dictionary<string, string> attributes)
        {
            this.x = x;
            this.y = y;
            this.id = id;
            this.edges = new List<Edge>();
            this.attributes = attributes;
        }
        #endregion

        /// <summary>
        /// Adds edge in edges list.
        /// </summary>
        /// <param name="e">edge to be added</param>
        public void AddEdge(Edge e)
        {
            edges.Add(e);
        }

        /// <summary>
        /// Removes edge from edges list.
        /// </summary>
        /// <param name="e">edge to be removed</param>
        public void RemoveEdge(Edge e)
        {
            edges.Remove(e);
        }
    }
}
