﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Layouts.Settings;

namespace GraphEditor.Core.Graphs
{
    /// <summary>
    /// GraphUtils class contains methods which manipulate graph some way.
    /// </summary>
    public static class GraphUtils
    {
        /// <summary>
        /// Updates borders of graph layout.
        /// 
        /// When ForceBorders from settings is set true, sets borders to values settings.MinX, etc.
        /// When ForceBorders from settings is set false, calculates borders from nodes position.
        /// </summary>
        /// <param name="graph">used graph</param>
        /// <param name="settings">used layout settings</param>
        public static void UpdateBorders(ref Graph graph, LayoutSettings settings)
        {
            if (settings.ForceBorders)
            {
                graph.SetAttribute("minX", settings.MinX.ToString());
                graph.SetAttribute("maxX", settings.MaxX.ToString());
                graph.SetAttribute("minY", settings.MinY.ToString());
                graph.SetAttribute("maxY", settings.MaxY.ToString());
            }
            else
            {
                double minX = double.PositiveInfinity;
                double maxX = double.NegativeInfinity;
                double minY = double.PositiveInfinity;
                double maxY = double.NegativeInfinity;

                foreach (Node n in graph.Nodes)
                {
                    minX = Math.Min(n.X, minX);
                    maxX = Math.Max(n.X, maxX);
                    minY = Math.Min(n.Y, minY);
                    maxY = Math.Max(n.Y, maxY);
                }

                graph.SetAttribute("minX", (minX - settings.Gap).ToString());
                graph.SetAttribute("maxX", (maxX + settings.Gap).ToString());
                graph.SetAttribute("minY", (minY - settings.Gap).ToString());
                graph.SetAttribute("maxY", (maxY + settings.Gap).ToString());
            }
        }
    }
}
