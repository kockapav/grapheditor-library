﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphEditor.Core.Graphs
{
    /// <summary>
    /// Class representing an Edge in the Graph.
    /// Has source and target Nodes and a Dictionary of optional attributes.
    /// </summary>
    public class Edge
    {
        private int id, source, target;
        private Dictionary<string, string> attributes;

        #region Getters and Setters
        /// <summary>
        /// Returns edge id.
        /// </summary>
        public int Id
        {
            get { return id; }
        }

        /// <summary>
        /// Returns id of source node.
        /// </summary>
        public int Source
        { 
            get { return source; } 
            set { this.source = value; } 
        }

        /// <summary>
        /// Returns id of target node.
        /// </summary>
        public int Target
        {
            get { return target; }
            set { this.target = value; }
        }

        /// <summary>
        /// Returns dictionary with all attributes.
        /// </summary>
        public Dictionary<string, string> Attributes
        {
            get { return attributes; }
        }

        /// <summary>
        /// Attribute value getter
        /// </summary>
        /// <param name="key">key (name) of attribute in string</param>
        /// <returns>value of attribute, null if key value is not found</returns>
        public string GetAttributeValue(string key)
        {
            string value;
            attributes.TryGetValue(key, out value);
            return value;
        }

        /// <summary>
        /// Sets attribute value.
        /// Creates a new attribute if key value does not exist.
        /// </summary>
        /// <param name="key">key (name) of attribute in string</param>
        /// <param name="value">value of attribute in string</param>
        public void SetAttribute(string key, string value)
        {
            attributes[key] = value;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Edge constructor
        /// </summary>
        /// <param name="source">Source node of edge</param>
        /// <param name="target">Target node of edge</param>
        public Edge(int id, int source, int target)
        {
            this.id = id;
            this.source = source;
            this.target = target;
            attributes = new Dictionary<string, string>();
        }

        /// <summary>
        /// Edge constructor
        /// </summary>
        /// <param name="source">Source node of edge</param>
        /// <param name="target">Target node of edge</param>
        /// <param name="attributes">Dictionary with optional attributes</param>
        public Edge(int id, int source, int target, Dictionary<string, string> attributes)
        {
            this.id = id;
            this.source = source;
            this.target = target;
            this.attributes = attributes;
        }
        #endregion
    }
}
