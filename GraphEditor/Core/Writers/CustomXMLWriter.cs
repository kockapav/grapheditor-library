﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using GraphEditor.Core.Graphs;
using System.IO;

namespace GraphEditor.Core.Writers
{
    /// <summary>
    /// Class used for saving graphs in custom simple XML format.
    /// </summary>
    public class CustomXMLWriter : IGraphWriter
    {
        private XmlWriter writer;
        private XmlWriterSettings settings;
        private StringWriter sw;

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CustomXMLWriter()
        {
            settings = new XmlWriterSettings();
            settings.NewLineHandling = NewLineHandling.Replace;
            settings.NewLineChars = "\r\n";
            settings.Indent = true;
        }
        #endregion

        #region Saving Methods
        /// <summary>
        /// Saves graph into file
        /// </summary>
        /// <param name="path">path to file which will be the graph saved in</param>
        /// <param name="graph">graph to be saved</param>
        /// <returns>true when writing graph into file finishes succesfully, else false</returns>
        public bool SaveToFile(string path, Graph graph)
        {
            try
            {
                writer = XmlWriter.Create(path, settings);
                SaveGraph(graph);
                writer.Close();
                return true;
            }
            catch (Exception)
            {
                if (writer != null) writer.Close();
                return false;
            }
        }

        /// <summary>
        /// Saves graph into string
        /// </summary>
        /// <param name="graph">graph to be saved</param>
        /// <returns>saved graph in string, null in case of error</returns>
        public string SaveToString(Graph graph)
        {
            try
            {
                sw = new StringWriter();
                writer = XmlWriter.Create(sw, settings);
                SaveGraph(graph);
                writer.Close();
                return sw.ToString();
            }
            catch (Exception)
            {
                if (writer != null) writer.Close();
                return null;
            }
        }

        /// <summary>
        /// Saves graph into xml file.
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveGraph(Graph graph)
        {
            writer.WriteStartDocument();
            // write header
            writer.WriteStartElement("graph");
            SaveAttributes(graph);
            SaveNodes(graph);
            SaveEdges(graph);
            writer.WriteEndElement();
            writer.WriteEndDocument();
        }

        /// <summary>
        /// Saves attributes into file
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveAttributes(Graph graph)
        {
            if (graph.Attributes.Count > 0)
            {
                writer.WriteStartElement("attributes");
                foreach (string name in graph.Attributes.Keys)
                {
                    writer.WriteStartElement("attribute");
                    writer.WriteAttributeString(name, graph.Attributes[name]);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Saves nodes into file
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveNodes(Graph graph)
        {
            if (graph.Nodes.Count > 0)
            {
                writer.WriteStartElement("nodes");
                foreach (Node node in graph.Nodes)
                {
                    // saves node
                    writer.WriteStartElement("node");
                    writer.WriteAttributeString("id", node.Id.ToString());
                    // saves node x coordinate
                    writer.WriteStartElement("attribute");
                    writer.WriteAttributeString("x", node.X.ToString());
                    writer.WriteEndElement();
                    // saves node y coordinate
                    writer.WriteStartElement("attribute");
                    writer.WriteAttributeString("y", node.Y.ToString());
                    writer.WriteEndElement();
                    // saves all other attributes
                    foreach (string name in node.Attributes.Keys)
                    {
                        writer.WriteStartElement("attribute");
                        writer.WriteAttributeString(name, node.Attributes[name]);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Saves edges into file
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveEdges(Graph graph)
        {
            if (graph.Edges.Count > 0)
            {
                writer.WriteStartElement("edges");
                foreach (Edge edge in graph.Edges)
                {
                    writer.WriteStartElement("edge");
                    writer.WriteAttributeString("id", edge.Id.ToString());
                    writer.WriteAttributeString("source", edge.Source.ToString());
                    writer.WriteAttributeString("target", edge.Target.ToString());
                    if (edge.Attributes.Count > 0)
                    {
                        foreach (string name in edge.Attributes.Keys)
                        {
                            writer.WriteStartElement("attribute");
                            writer.WriteAttributeString(name, edge.Attributes[name]);
                            writer.WriteEndElement();
                        }
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        #endregion
    }
}
