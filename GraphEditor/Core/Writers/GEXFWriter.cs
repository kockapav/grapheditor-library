﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using GraphEditor.Core.Graphs;
using System.IO;

namespace GraphEditor.Core.Writers
{
    /// <summary>
    /// Class used for saving graphs to .gexf file.
    /// 
    /// Does support string attributes (no default and option values, different types are converted to string type).
    /// Does support meta element (with creator, description and keywords).
    /// Does not support advanced GEXF options like nested nodes and edges, etc.
    /// Does not support VIZ extension.
    /// </summary>
    public class GEXFWriter : IGraphWriter
    {
        private XmlWriter writer;
        private XmlWriterSettings settings;
        private StringWriter sw;

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public GEXFWriter()
        {
            this.settings = new XmlWriterSettings();
            this.settings.NewLineHandling = NewLineHandling.Replace;
            this.settings.NewLineChars = "\r\n";
            this.settings.Indent = true;
        }
        #endregion

        #region Saving methods
        /// <summary>
        /// Attempts to save graph to file in gexf format.
        /// </summary>
        /// <param name="path">path and file name for saved graph</param>
        /// <param name="graph">graph to be saved</param>
        /// <returns>true when successful, else false</returns>
        public bool SaveToFile(string path, Graph graph)
        {
            try
            {
                writer = XmlWriter.Create(path, settings);
                SaveGraph(graph);
                writer.Close();
                return true;
            }
            catch (Exception)
            {
                if (writer != null) writer.Close();
                return false;
            }
        }

        /// <summary>
        /// Saves graph into string in .gexf format
        /// </summary>
        /// <param name="graph">graph to be saved</param>
        /// <returns>saved graph in string, null in case of error</returns>
        public string SaveToString(Graph graph)
        {
            try
            {
                sw = new StringWriter();
                writer = XmlWriter.Create(sw, settings);
                SaveGraph(graph);
                writer.Close();
                return sw.ToString();
            }
            catch (Exception)
            {
                if (writer != null) writer.Close();
                return null;
            }
        }

        /// <summary>
        /// Saves graph into gexf file.
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveGraph(Graph graph)
        {
            writer.WriteStartDocument();
            // write header
            writer.WriteStartElement("gexf", "http://www.gexf.net/1.2draft");
            writer.WriteAttributeString("xmlns", "viz", null, "http://www.gexf.net/1.2draft/viz");
            writer.WriteAttributeString("version", "1.2");
            SaveMetaData(graph);
            // write graph
            writer.WriteStartElement("graph");
            WriteAttribute(graph, "mode");
            WriteAttribute(graph, "defaultedgetype");
            SaveNodeAttributes(graph);
            SaveEdgeAttributes(graph);
            SaveNodes(graph);
            SaveEdges(graph);

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndDocument();
        }

        /// <summary>
        /// Saves node attributes into opened gexf file.
        /// Expects all nodes to have same attributes.
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveNodeAttributes(Graph graph)
        {
            if (graph.Nodes.Count == 0) return;
            Dictionary<string, string> attributes = graph.Nodes[0].Attributes;
            writer.WriteStartElement("attributes");
            writer.WriteAttributeString("class", "node");

            // write x and y coordinate attributes
            writer.WriteStartElement("attribute");
            writer.WriteAttributeString("id", "0");
            writer.WriteAttributeString("title", "x");
            writer.WriteAttributeString("type", "string");
            writer.WriteEndElement();
            writer.WriteStartElement("attribute");
            writer.WriteAttributeString("id", "1");
            writer.WriteAttributeString("title", "y");
            writer.WriteAttributeString("type", "string");
            writer.WriteEndElement();

            // write other attributes
            for (int i = 0; i < attributes.Count; i++)
            {
                writer.WriteStartElement("attribute");
                writer.WriteAttributeString("id", (i + 2).ToString());
                writer.WriteAttributeString("title", attributes.ElementAt(i).Key);
                writer.WriteAttributeString("type", "string");
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        /// <summary>
        /// Saves edge attributes into opened gexf file.
        /// Expects all edges to have same attributes.
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveEdgeAttributes(Graph graph)
        {
            if (graph.Edges.Count == 0) return;
            Dictionary<string, string> attributes = graph.Edges[0].Attributes;
            if (attributes.Count > 0)
            {
                writer.WriteStartElement("attributes");
                writer.WriteAttributeString("class", "edge");
                for (int i = 0; i < attributes.Count; i++)
                {
                    writer.WriteStartElement("attribute");
                    writer.WriteAttributeString("id", i.ToString());
                    writer.WriteAttributeString("title", attributes.ElementAt(i).Key);
                    writer.WriteAttributeString("type", "string");
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Saves nodes into file.
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveNodes(Graph graph)
        {
            if (graph.Nodes.Count == 0) return;
            writer.WriteStartElement("nodes");
            foreach (Node node in graph.Nodes)
            {
                writer.WriteStartElement("node");
                writer.WriteAttributeString("id", node.Id.ToString());
                writer.WriteAttributeString("label", node.GetAttributeValue("label"));
                SaveNodeVIZ(node);
                writer.WriteStartElement("attvalues");
                // write x and y coordinate attributes
                writer.WriteStartElement("attvalue");
                writer.WriteAttributeString("for", "0");
                writer.WriteAttributeString("value", node.X.ToString());
                writer.WriteEndElement();
                writer.WriteStartElement("attvalue");
                writer.WriteAttributeString("for", "1");
                writer.WriteAttributeString("value", node.Y.ToString());
                writer.WriteEndElement();
                // write other attributes
                for (int i = 0; i < node.Attributes.Count; i++)
                {
                    writer.WriteStartElement("attvalue");
                    writer.WriteAttributeString("for", (i + 2).ToString());
                    writer.WriteAttributeString("value", node.Attributes.ElementAt(i).Value);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        /// <summary>
        /// Saves node VIZ plugin info
        /// </summary>
        /// <param name="node">used node</param>
        private void SaveNodeVIZ(Node node)
        {
            // write node color values -- viz:color
            string tmp = node.GetAttributeValue("r");
            if (tmp != null)
            {
                writer.WriteStartElement("viz", "color", null);
                writer.WriteAttributeString("r", tmp);
                writer.WriteAttributeString("g", node.GetAttributeValue("g"));
                writer.WriteAttributeString("b", node.GetAttributeValue("b"));
                tmp = node.GetAttributeValue("a");
                if (tmp != null) writer.WriteAttributeString("a", tmp);
                writer.WriteEndElement();
            }

            // write node coordinates
            writer.WriteStartElement("viz", "position", null);
            writer.WriteAttributeString("x", XmlConvert.ToString(node.X));
            writer.WriteAttributeString("y", XmlConvert.ToString(node.Y));
            string z = node.GetAttributeValue("z");
            if (z == null)
            {
                writer.WriteAttributeString("z", "0");
            }
            else
            {
                writer.WriteAttributeString("z", z);
            }
            writer.WriteEndElement();

            // write node size
            tmp = node.GetAttributeValue("size");
            if (tmp != null)
            {
                writer.WriteStartElement("viz", "size", null);
                writer.WriteAttributeString("value",tmp);
                writer.WriteEndElement();
            }

            // write node shape
            tmp = node.GetAttributeValue("shape");
            if (tmp != null)
            {
                writer.WriteStartElement("viz", "shape", null);
                writer.WriteAttributeString("value", tmp);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Saves edges into file.
        /// </summary>
        /// <param name="edge">saved graph</param>
        private void SaveEdges(Graph graph)
        {
            if (graph.Edges.Count == 0) return;
            writer.WriteStartElement("edges");
            foreach (Edge edge in graph.Edges)
            {
                writer.WriteStartElement("edge");

                writer.WriteAttributeString("id", edge.Id.ToString());
                writer.WriteAttributeString("source", edge.Source.ToString());
                writer.WriteAttributeString("target", edge.Target.ToString());

                SaveEdgeVIZ(edge);

                if (edge.Attributes.Count > 0)
                {
                    writer.WriteStartElement("attvalues");
                    for (int i = 0; i < edge.Attributes.Count; i++)
                    {
                        writer.WriteStartElement("attvalue");
                        writer.WriteAttributeString("for", i.ToString());
                        writer.WriteAttributeString("value", edge.Attributes.ElementAt(i).Value);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        /// <summary>
        /// Saves edge VIZ plugin info
        /// </summary>
        /// <param name="edge">used edge</param>
        private void SaveEdgeVIZ(Edge edge)
        {
            // write edge color values -- viz:color
            string tmp = edge.GetAttributeValue("r");
            if (tmp != null)
            {
                writer.WriteStartElement("viz", "color", null);
                writer.WriteAttributeString("r", tmp);
                writer.WriteAttributeString("g", edge.GetAttributeValue("g"));
                writer.WriteAttributeString("b", edge.GetAttributeValue("b"));
                tmp = edge.GetAttributeValue("a");
                if (tmp != null) writer.WriteAttributeString("a", tmp);
                writer.WriteEndElement();
            }

            // write edge thickness
            tmp = edge.GetAttributeValue("thickness");
            if (tmp != null)
            {
                writer.WriteStartElement("viz", "thickness", null);
                writer.WriteAttributeString("value", tmp);
                writer.WriteEndElement();
            }

            // write edge shape
            tmp = edge.GetAttributeValue("shape");
            if (tmp != null)
            {
                writer.WriteStartElement("viz", "shape", null);
                writer.WriteAttributeString("value", tmp);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Attempts to save metadata into file.
        /// </summary>
        /// <param name="graph">saved graph</param>
        private void SaveMetaData(Graph graph)
        {
            string value;
            string date = graph.GetAttributeValue("lastmodifieddate");
            if (date != null)
            {
                writer.WriteStartElement("meta");
                writer.WriteAttributeString("lastmodifieddate", date);
                string[] attributes = new string[3] { "creator", "description", "keywords" };
                foreach (string attr in attributes)
                {
                    value = graph.GetAttributeValue(attr);
                    if (value != null)
                    {
                        writer.WriteStartElement(attr);
                        writer.WriteString(value);
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Writes graph attribute into file.
        /// If the attribute is not present nothing happens.
        /// </summary>
        /// <param name="graph">saved graph</param>
        /// <param name="attr">attribute name in graph attributes</param>
        private void WriteAttribute(Graph graph, string attr)
        {
            string value = graph.GetAttributeValue(attr);
            if (value != null)
            {
                writer.WriteAttributeString(attr, value);
            }
        }
        #endregion
    }
}
