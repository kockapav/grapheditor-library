﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GraphEditor.Core.Writers
{
    /// <summary>
    /// WriterFactory class,
    /// initializes specifid IGraphWriter implementation based on chosen file format
    /// </summary>
    public class WriterFactory
    {
        /// <summary>
        /// Returns file writer based on file extension.
        /// </summary>
        /// <param name="file">path and file name</param>
        /// <returns>graph writer</returns>
        public IGraphWriter GetWriterFromPath(string file)
        {
            string ext = Path.GetExtension(file);
            switch (ext)
            {
                case ".gexf": return GetWriter(0);
                case ".xml":
                default: return GetWriter(1);
            }
        }

        /// <summary>
        /// Returns file writer based on format id (FormatsList.xml)
        /// </summary>
        /// <param name="id">id of writer to be created</param>
        /// <returns>chosen graph writer</returns>
        public IGraphWriter GetWriter(int id)
        {
            switch (id)
            {
                case 0: return new GEXFWriter();
                case 1:
                default: return new CustomXMLWriter();
            }
        }
    }
}
