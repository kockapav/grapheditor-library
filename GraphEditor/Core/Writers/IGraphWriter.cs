﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Writers
{
    /// <summary>
    /// Interface used for graph Writers (saving to file)
    /// </summary>
    public interface IGraphWriter
    {
        /// <summary>
        /// Saves graph into file
        /// </summary>
        /// <param name="path">path to file which will be the graph saved in</param>
        /// <param name="graph">graph to be saved</param>
        /// <returns>true when writing graph into file finishes succesfully, else false</returns>
        bool SaveToFile(string path, Graph graph);

        /// <summary>
        /// Saves graph into string
        /// </summary>
        /// <param name="graph">graph to be saved</param>
        /// <returns>saved graph in string</returns>
        string SaveToString(Graph graph);
    }
}
