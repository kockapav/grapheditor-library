﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphEditor.Core.Graphs;

namespace GraphEditor.Core.Parsers
{
    /// <summary>
    /// Interface used for graph Parsers.
    /// </summary>
    public interface IGraphParser
    {
        /// <summary>
        /// Parses graph from file.
        /// </summary>
        /// <param name="id">id of new graph</param>
        /// <param name="path">path to file</param>
        /// <returns>parsed graph</returns>
        Graph LoadFromFile(int id, string path);

        /// <summary>
        /// Parses graph from string
        /// </summary>
        /// <param name="id">id of new graph</param>
        /// <param name="graph">string with graph specification</param>
        /// <returns>parsed graph</returns>
        Graph LoadFromString(int id, string graph);
    }
}
