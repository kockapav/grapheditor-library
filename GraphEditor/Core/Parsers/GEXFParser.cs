﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using GraphEditor.Core.Graphs;
using System.IO;

namespace GraphEditor.Core.Parsers
{
    /// <summary>
    /// Class used for parsing graphs from basic GEXF (gephi graph xml).
    /// 
    /// Does support string attributes (no default and option values, different types are converted to string type).
    /// Does support meta element (with creator, description and keywords).
    /// Does not support advanced GEXF options like nested nodes and edges, etc.
    /// Does not support VIZ extension.
    /// </summary>
    public class GEXFParser : IGraphParser
    {
        private XmlReader reader;
        private XmlReaderSettings settings;
        private Dictionary<string, string> nodeAttributes;
        private Dictionary<string, string> edgeAttributes;
        private bool nodeHasAttr;
        private bool edgeHasAttr;

        #region Constructors
        /// <summary>
        /// Default constructor, reader ignores comments and whitespaces
        /// </summary>
        public GEXFParser()
        {
            this.nodeHasAttr = false;
            this.edgeHasAttr = false;
            this.settings = new XmlReaderSettings();
            this.settings = new XmlReaderSettings();
            this.settings.IgnoreComments = true;
            this.settings.IgnoreWhitespace = true;
            this.settings.IgnoreProcessingInstructions = true;
        }
        #endregion

        #region Parsing methods
        /// <summary>
        /// Loads a graph from .gexf file.
        /// </summary>
        /// <param name="id">id of new graph</param>
        /// <param name="path">path to file</param>
        /// <returns>loaded graph when successful, null when failed.</returns>
        public Graph LoadFromFile(int id, string path)
        {
            Graph graph = new Graph(id);
            try
            {
                reader = XmlReader.Create(path, settings);
                LoadGEXF(ref graph);
                reader.Close();
                return graph;
            }
            catch (Exception)
            {
                // error while opening / reading file or while parsing
                if(reader != null) reader.Close();
                return null;
            }
        }

        /// <summary>
        /// Parses graph from string in .gexf format.
        /// </summary>
        /// <param name="id">id of new graph</param>
        /// <param name="graph">string with graph in .gexf</param>
        /// <returns>parsed graph</returns>
        public Graph LoadFromString(int id, string graphString)
        {
            Graph graph = new Graph(id);
            try
            {
                reader = XmlReader.Create(new StringReader(graphString), settings);
                LoadGEXF(ref graph);
                reader.Close();
                return graph;
            }
            catch (Exception)
            {
                // error while opening / reading file or while parsing
                if (reader != null) reader.Close();
                return null;
            }
        }

        /// <summary>
        /// Loads graph from loaded xml file.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadGEXF(ref Graph graph)
        {
            // skip until you reach gexf element
            while (reader.Name != "gexf")
            {
                if (reader.EOF) throw new InvalidOperationException("Error while parsing gexf file.");
                reader.Read();
            }

            // skip until you reach graph specs
            // try to parse meta element if found
            while (reader.Name != "graph")
            {
                reader.Read();
                if (reader.Name == "meta") ReadMetaData(ref graph);
                if (reader.EOF) throw new InvalidOperationException("Error while parsing gexf file.");
            }

            // try to parse graph structure
            LoadGraph(ref graph);
        }

        /// <summary>
        /// Attempts to read graph structure.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadGraph(ref Graph graph)
        {
            // save graph attributes
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToNextAttribute();
                graph.SetAttribute(reader.Name, reader.Value);
            }

            // parse graph
            while(!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "graph"))
            {
                reader.Read();
                switch (reader.Name)
                {
                    case "attributes":
                        LoadAttributes(ref graph);
                        break;
                    case "nodes":
                        LoadNodes(ref graph);
                        break;
                    case "edges":
                        LoadEdges(ref graph);
                        break;
                }
            }
        }

        /// <summary>
        /// Attempts to load attributes list.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadAttributes(ref Graph graph)
        {
            Dictionary<string, string> attr = new Dictionary<string, string>();
            // decide if you are loading node or edge attributes
            if (reader.GetAttribute("class") == "node")
            {
                nodeHasAttr = true;
                nodeAttributes = attr;
            }
            else
            {
                edgeHasAttr = true;
                edgeAttributes = attr;
            }
            // load attributes list
            while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "attributes"))
            {
                reader.Read();
                if (reader.Name == "attribute") attr.Add(reader.GetAttribute("id"), reader.GetAttribute("title"));
            }
        }

        /// <summary>
        /// Attempts to load nodes list.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadNodes(ref Graph graph)
        {
            while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "nodes"))
            {
                reader.Read();
                if (reader.Name == "node") LoadOneNode(ref graph);
            }
        }

        /// <summary>
        /// Attempts to load one single node.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadOneNode(ref Graph graph)
        {
            Node node = new Node(int.Parse((reader.GetAttribute("id"))));
            node.SetAttribute("label", reader.GetAttribute("label"));
            if (!reader.IsEmptyElement)
            {
                while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "node"))
                {
                    reader.Read();
                    if (reader.Name != "attvalue") parseVizNodeElement(ref node); 
                    if (nodeHasAttr && reader.Name == "attvalue")
                    {
                        string name, value;
                        nodeAttributes.TryGetValue(reader.GetAttribute("for"), out name);
                        value = reader.GetAttribute("value");
                        if (name == "x")
                        {
                            node.X = double.Parse(value);
                        }
                        else if (name == "y")
                        {
                            node.Y = double.Parse(value);
                        }
                        else
                        {
                            node.SetAttribute(name, value);
                        }
                    }
                }
            }
            graph.AddNode(node);
        }

        /// <summary>
        /// Parses viz plugin node element
        /// </summary>
        /// <param name="node">used node object</param>
        private void parseVizNodeElement(ref Node node)
        {
            switch (reader.Name)
            {
                case "viz:color":
                    string color = reader.GetAttribute("r");
                    if (color != null) node.SetAttribute("r", color);
                    color = reader.GetAttribute("g");
                    if (color != null) node.SetAttribute("g", color);
                    color = reader.GetAttribute("b");
                    if (color != null) node.SetAttribute("b", color);
                    color = reader.GetAttribute("a");
                    if (color != null) node.SetAttribute("a", color);
                    break;
                case "viz:position":
                    string coord = reader.GetAttribute("x");
                    node.X = XmlConvert.ToDouble(coord);
                    coord = reader.GetAttribute("y");
                    node.Y = XmlConvert.ToDouble(coord);
                    coord = reader.GetAttribute("z");
                    node.SetAttribute("z", coord);
                    break;
                case "viz:size":
                    string size = reader.GetAttribute("value");
                    node.SetAttribute("size", size);
                    break;
                case "viz:shape":
                    string shape = reader.GetAttribute("shape");
                    node.SetAttribute("shape", shape);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Attempts to load edges list.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadEdges(ref Graph graph)
        {
            while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "edges"))
            {
                reader.Read();
                if (reader.Name == "edge") LoadOneEdge(ref graph);
            }
        }

        /// <summary>
        /// Attempts to load one single edge.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadOneEdge(ref Graph graph)
        {
            Edge edge = new Edge(int.Parse(reader.GetAttribute("id")), int.Parse((reader.GetAttribute("source"))), int.Parse((reader.GetAttribute("target"))));
            if (!reader.IsEmptyElement)
            {
                while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "edge"))
                {
                    reader.Read();
                    if (reader.Name != "attvalue") parseVizEdgeElement(ref edge);
                    if (edgeHasAttr && reader.Name == "attvalue")
                    {
                        string name, value;
                        nodeAttributes.TryGetValue(reader.GetAttribute("for"), out name);
                        value = reader.GetAttribute("value");
                        edge.SetAttribute(name, value);
                    }
                }
            }
            graph.AddEdge(edge);
        }

        /// <summary>
        /// Parses viz plugin edge element
        /// </summary>
        /// <param name="edge">used edge</param>
        private void parseVizEdgeElement(ref Edge edge)
        {
            switch (reader.Name)
            {
                case "viz:color":
                    string color = reader.GetAttribute("r");
                    if (color != null) edge.SetAttribute("r", color);
                    color = reader.GetAttribute("g");
                    if (color != null) edge.SetAttribute("g", color);
                    color = reader.GetAttribute("b");
                    if (color != null) edge.SetAttribute("b", color);
                    color = reader.GetAttribute("a");
                    if (color != null) edge.SetAttribute("a", color);
                    break;
                case "viz:thickness":
                    string thick = reader.GetAttribute("value");
                    edge.SetAttribute("thickness", thick);
                    break;
                case "viz:shape":
                    string shape = reader.GetAttribute("shape");
                    edge.SetAttribute("shape", shape);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Attempts to read data from meta element.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void ReadMetaData(ref Graph graph)
        {
            reader.MoveToNextAttribute();
            graph.SetAttribute(reader.Name, reader.Value);
            while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "meta"))
            {
                string name;
                string value;
                reader.Read();
                if (reader.Name != "meta")
                {
                    name = reader.Name;
                    reader.Read();
                    value = reader.Value;
                    reader.Read();
                    graph.SetAttribute(name, value);
                }
            }
        }
        #endregion
    }
}
