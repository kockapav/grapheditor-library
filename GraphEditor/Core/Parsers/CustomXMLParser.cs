﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using GraphEditor.Core.Graphs;
using System.IO;

namespace GraphEditor.Core.Parsers
{
    /// <summary>
    /// Class used for parsing custom simple XML graph format.
    /// </summary>
    public class CustomXMLParser : IGraphParser
    {
        private XmlReader reader;
        private XmlReaderSettings settings;

        #region Constructors
        /// <summary>
        /// Default constructor, initializes XmlReaderSettings,
        /// reader ignores comments and whitespaces
        /// </summary>
        public CustomXMLParser()
        {
            this.settings = new XmlReaderSettings();
            this.settings = new XmlReaderSettings();
            this.settings.IgnoreComments = true;
            this.settings.IgnoreWhitespace = true;
            this.settings.IgnoreProcessingInstructions = true;
        }
        #endregion

        #region Parsing Methods
        /// <summary>
        /// Parses graph from file.
        /// </summary>
        /// <param name="id">id of new graph</param>
        /// <param name="path">path to file</param>
        /// <returns>parsed graph</returns>
        public Graph LoadFromFile(int id, string path)
        {
            Graph graph = new Graph(id);
            try
            {
                reader = XmlReader.Create(path, settings);
                LoadCustomXML(ref graph);
                reader.Close();
                return graph;
            }
            catch (Exception)
            {
                // error while opening / reading file or while parsing
                if (reader != null) reader.Close();
                return null;
            }
        }

        /// <summary>
        /// Parses graph from string
        /// </summary>
        /// <param name="id">id of new graph</param>
        /// <param name="graph">string with graph specification</param>
        /// <returns>parsed graph</returns>
        public Graph LoadFromString(int id, string graphString)
        {
            Graph graph = new Graph(id);
            try
            {
                reader = XmlReader.Create(new StringReader(graphString), settings);
                LoadCustomXML(ref graph);
                reader.Close();
                return graph;
            }
            catch (Exception)
            {
                // error while opening / reading file or while parsing
                if (reader != null) reader.Close();
                return null;
            }
        }

        /// <summary>
        /// Loads graph from xml file.
        /// </summary>
        /// <param name="Graph">used graph object</param>
        private void LoadCustomXML(ref Graph graph)
        {
            while (reader.Name != "graph") reader.Read();
            reader.Read();
            LoadGraphAttributes(ref graph);
            LoadNodes(ref graph);
            LoadEdges(ref graph);
        }

        /// <summary>
        /// Loads graph attributes.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadGraphAttributes(ref Graph graph)
        {
            if (reader.Name == "attributes") 
            {
                while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "attributes"))
                {
                    reader.Read();
                    if (reader.Name == "attribute") graph.SetAttribute(reader.GetAttribute("name"), reader.GetAttribute("value"));
                }
                reader.Read();
            }
        }

        /// <summary>
        /// Loads graph nodes.
        /// </summary>
        /// <param name="graph">used graph object</param>
        private void LoadNodes(ref Graph graph)
        {
            if (reader.Name == "nodes")
            {
                while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "nodes"))
                {
                    reader.Read();
                    if (reader.Name == "node")
                    {
                        Node node = new Node(int.Parse(reader.GetAttribute("id")));
                        while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "node"))
                        {
                            reader.Read();
                            if (reader.Name == "attribute")
                            {
                                string name = reader.GetAttribute("name");
                                if (name == "x") 
                                {
                                    node.X = double.Parse(reader.GetAttribute("value"));
                                }
                                else if (name == "y")
                                {
                                    node.Y = double.Parse(reader.GetAttribute("value"));
                                }
                                else
                                {
                                    node.SetAttribute(reader.GetAttribute("name"), reader.GetAttribute("value"));
                                }
                            }
                        }
                        graph.AddNode(node);
                    }
                }
                reader.Read();
            }
        }

        private void LoadEdges(ref Graph graph)
        {
            if (reader.Name == "edges")
            {
                while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "edges"))
                {
                    reader.Read();
                    if (reader.Name == "edge")
                    {
                        int id = int.Parse(reader.GetAttribute("id"));
                        int source = int.Parse(reader.GetAttribute("source"));
                        int target = int.Parse(reader.GetAttribute("target"));
                        Edge edge = new Edge(id, source, target);
                        if (!reader.IsEmptyElement)
                        {
                            while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == "edge"))
                            {
                                reader.Read();
                                if (reader.Name == "attribute") edge.SetAttribute(reader.GetAttribute("name"), reader.GetAttribute("value"));
                            }
                        }
                        graph.AddEdge(edge);
                    }
                }
                reader.Read();
            }
        }
        #endregion
    }
}
