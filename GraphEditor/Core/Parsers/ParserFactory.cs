﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GraphEditor.Core.Parsers
{
    /// <summary>
    /// ParserFactory class,
    /// initializes specifid IGraphParser implementation based on chosen file format
    /// </summary>
    public class ParserFactory
    {
        /// <summary>
        /// Returns file parser based on file extension.
        /// </summary>
        /// <param name="file">path and file name</param>
        /// <returns>graph parser</returns>
        public IGraphParser GetParserFromPath(string file)
        {
            string ext = Path.GetExtension(file);
            switch (ext)
            {
                case ".gexf": return GetParser(0);
                case ".xml":
                default: return GetParser(1);
            }
        }

        /// <summary>
        /// Returns file parser based on format id (FormatsList.xml)
        /// </summary>
        /// <param name="id">id of parser to be created</param>
        /// <returns>chosen graph parser</returns>
        public IGraphParser GetParser(int id)
        {
            switch (id)
            {
                case 0: return new GEXFParser();
                case 1:
                default: return new CustomXMLParser();
            }
        }
    }
}
