﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GraphEditor.Core.Graphs;
using GraphEditor.Core.Parsers;
using GraphEditor.Core.Writers;
using GraphEditor.Core.Layouts;
using GraphEditor.Core.Layouts.Settings;


namespace GraphEditor
{
    /// <summary>
    /// GraphEditor interface, contains definitions for all required methods used for editing graphs and its layout
    /// </summary>
    [ComVisible(true)]
    [Guid("34D2C607-AEDE-4A87-AEDC-BB4F867C6064")]
    public interface IGraphEditor
    {
        /// <summary>
        /// Returns the version of current assembly.
        /// </summary>
        /// <returns>version</returns>
        string GetVersion();

        /// <summary>
        /// Returns list of supported layouts.
        /// </summary>
        /// <returns>list of supported layouts in string</returns>
        string GetLayoutList();

        /// <summary>
        /// Returns list of supported file formats.
        /// </summary>
        /// <returns>list of supported file formats in string</returns>
        string GetFormatList();

        /// <summary>
        /// Loads graph from file.
        /// Recognizes file format based on file extension.
        /// </summary>
        /// <param name="file">path to file and file name</param>
        /// <returns>id of loaded graph >= 0, returns -1 when loading failed. </returns>
        int LoadGraphFromFile(string file);

        /// <summary>
        /// Loads graph from file.
        /// </summary>
        /// <param name="file">path to file and file name</param>
        /// <param name="formatId">id of format to be used when parsing file</param>
        /// <returns>id of loaded graph >= 0, returns -1 when loading failed.</returns>
        int LoadGraphFromFile(string file, int formatId);

        /// <summary>
        /// Loads graph from string.
        /// </summary>
        /// <param name="file">string with graph specification</param>
        /// <param name="formatId">id of format to be used when parsing graph</param>
        /// <returns>id of loaded graph >= 0, returns -1 when loading failed.</returns>
        int LoadGraphFromString(string graphString, int formatId);

        /// <summary>
        /// Saves graph to file.
        /// Recognizes file format based on file extension.
        /// </summary>
        /// <param name="file">path to file and file name in which the graph will be saved</param>
        /// <returns>true when saved successfully, else false</returns>
        bool SaveGraphToFile(int graphId, string file);

        /// <summary>
        /// Saves graph to file.
        /// Recognizes file format based on id.
        /// </summary>
        /// <param name="file">path to file and file name in which the graph will be saved</param>
        /// <param name="formatId">id of format to be used when saving file</param>
        /// <returns>true when saved successfully, else false</returns>
        bool SaveGraphToFile(int graphId, string file, int formatId);

        /// <summary>
        /// Gets graph in string
        /// </summary>
        /// <param name="graphId">id of graph to be returned</param>
        /// <param name="formatId">id of format in which should graph be returned</param>
        /// <returns>graph in string</returns>
        string SaveGraphToString(int graphId, int formatId);

        /// <summary>
        /// Changes graph layout to one specified by id
        /// </summary>
        /// <param name="graphId">id of graph to be changed</param>
        /// <param name="layoutId">id of layout to be used</param>
        /// <returns>true when successfull, else false</returns>
        bool SetGraphLayout(int graphId, int layoutId);

        /// <summary>
        /// Changes graph layout to one specified by id
        /// </summary>
        /// <param name="graphId">id of graph to be changed</param>
        /// <param name="layoutId">id of layout to be used</param>
        /// <param name="settings">layout settings</param>
        /// <returns>true when successfull, else false</returns>
        bool SetGraphLayout(int graphId, int layoutId, LayoutSettings settings);

        /// <summary>
        /// Removes loaded graph from memory.
        /// WARNING: unsaved changes in graph will be lost.
        /// </summary>
        /// <param name="graphId">id of graph to be removed</param>
        void RemoveGraphFromMemory(int graphId);
    }

    /// <summary>
    /// GraphEditor class, implements IGraphEditor
    /// </summary>
    [ComVisible(true)]
    [Guid("7126823D-B0E5-42B3-8A98-6378BC3254CE")]
    public class GraphEditor : IGraphEditor
    {
        private List<Graph> graphs;
        // graphCnt is used for assigning id's to graphs, value can only increase to avoid id duplicities
        private int graphCnt;

        #region Getters and Setters
        /// <summary>
        /// Returns list with all loaded graphs
        /// </summary>
        public List<Graph> Graphs
        {
            get { return graphs; }
        }
        #endregion

        #region Constructors
        public GraphEditor()
        {
            graphs = new List<Graph>();
            graphCnt = 0;
        }
        #endregion

        /// <summary>
        /// Returns the version of current assembly.
        /// </summary>
        /// <returns>version</returns>
        public string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Returns list of all supported layouts in XML format.
        /// </summary>
        /// <returns>list of layouts in string (xml)</returns>
        public string GetLayoutList()
        {
            return GetResourceFileString("GraphEditor.Resources.LayoutsList.xml");
        }

        /// <summary>
        /// Returns list of supported file formats in XML.
        /// </summary>
        /// <returns>list of supported file formats in string (xml)</returns>
        public string GetFormatList()
        {
            return GetResourceFileString("GraphEditor.Resources.FormatsList.xml");
        }

        /// <summary>
        /// Loads resource file in memory and returns it in string
        /// </summary>
        /// <param name="file">namespace.path.resource_name.extension</param>
        /// <returns>loaded resource in string</returns>
        private string GetResourceFileString(string file)
        {
            string resource;
            using (Stream s = GetType().Module.Assembly.GetManifestResourceStream(file))
            {
                using (StreamReader sw = new StreamReader(s))
                {
                    resource = sw.ReadToEnd();
                }
            }
            return resource;
        }

        #region Loading Graphs
        /// <summary>
        /// Loads graph from file.
        /// Tries to decide file format from file extension.
        /// Default file format id = 0.
        /// </summary>
        /// <param name="file">path to file and file name</param>
        /// <returns>id of loaded graph >= 0, returns -1 when loading failed. </returns>
        public int LoadGraphFromFile(string file)
        {
            return LoadGraph(file, new ParserFactory().GetParserFromPath(file));
        }

        /// <summary>
        /// Loads graph from file.
        /// </summary>
        /// <param name="file">path to file and file name</param>
        /// <param name="formatId">id of format to be used when parsing file</param>
        /// <returns>id of loaded graph >= 0, returns -1 when loading failed.</returns>
        public int LoadGraphFromFile(string file, int formatId)
        {
            return LoadGraph(file, new ParserFactory().GetParser(formatId));
        }

        /// <summary>
        /// Loads graph from file with giver parser
        /// </summary>
        /// <param name="file">path to file and file name</param>
        /// <param name="parser">initialized graph parser implementation</param>
        /// <returns>id of loaded graph >= 0, returns -1 when loading failed.</returns>
        private int LoadGraph(string file, IGraphParser parser)
        {
            Graph g = parser.LoadFromFile(graphCnt, file);
            if (g == null) return -1;
            graphs.Add(g);
            graphCnt++;
            return g.Id;
        }

        /// <summary>
        /// Loads graph from string.
        /// </summary>
        /// <param name="file">string with graph specification</param>
        /// <param name="formatId">id of format to be used when parsing graph</param>
        /// <returns>id of loaded graph >= 0, returns -1 when loading failed.</returns>
        public int LoadGraphFromString(string graphString, int formatId)
        {
            IGraphParser parser = new ParserFactory().GetParser(formatId);
            Graph g = parser.LoadFromString(graphCnt, graphString);
            if (g == null) return -1;
            graphs.Add(g);
            graphCnt++;
            return g.Id;
        }
        #endregion

        #region Saving Graphs
        /// <summary>
        /// Saves graph to file.
        /// Recognizes file format based on file extension.
        /// </summary>
        /// <param name="id">id of graph to be saved</param>
        /// <param name="file">path to file and file name in which the graph will be saved</param>
        /// <returns>true when saved successfully, else false</returns>
        public bool SaveGraphToFile(int graphId, string file)
        {
            IGraphWriter writer = new WriterFactory().GetWriterFromPath(file);
            return writer.SaveToFile(file, graphs.Find(graph => graph.Id == graphId));
        }

        /// <summary>
        /// Saves graph to file.
        /// Recognizes file format based on id.
        /// </summary>
        /// <param name="id">id of graph to be saved</param>
        /// <param name="file">path to file and file name in which the graph will be saved</param>
        /// <param name="formatId">id of format to be used when saving file</param>
        /// <returns>true when saved successfully, else false</returns>
        public bool SaveGraphToFile(int graphId, string file, int formatId)
        {
            IGraphWriter writer = new WriterFactory().GetWriter(formatId);
            return writer.SaveToFile(file, graphs.Find(graph => graph.Id == graphId));
        }

        /// <summary>
        /// Gets graph in string
        /// </summary>
        /// <param name="graphId">id of graph to be returned</param>
        /// <param name="formatId">id of format in which should graph be returned</param>
        /// <returns>graph in string</returns>
        public string SaveGraphToString(int graphId, int formatId)
        {
            IGraphWriter writer = new WriterFactory().GetWriter(formatId);
            return writer.SaveToString(graphs.Find(graph => graph.Id == graphId));
        }
        #endregion

        #region Changing Graphs
        /// <summary>
        /// Sets graph layout to one specified by id.
        /// If setting layout fails, the original graph remains unchanged.
        /// </summary>
        /// <param name="graphId">id of graph to be changed</param>
        /// <param name="layoutId">id of layout to be used</param>
        /// <returns>true when successfull, else false</returns>
        public bool SetGraphLayout(int graphId, int layoutId)
        {
            bool result;
            IGraphLayout layout = new LayoutFactory().GetLayout(layoutId);
            int index = graphs.FindIndex(graph => graph.Id == graphId);
            Graph g = graphs[index];
            result = layout.SetLayout(ref g);
            if (result) graphs[index] = g;
            return result;
        }

        /// <summary>
        /// Sets graph layout to one specified by id.
        /// If setting layout fails, the original graph remains unchanged.
        /// </summary>
        /// <param name="graphId">id of graph to be changed</param>
        /// <param name="layoutId">id of layout to be used</param>
        /// <param name="settings">layout settings</param>
        /// <returns>true when successfull, else false</returns>
        public bool SetGraphLayout(int graphId, int layoutId, LayoutSettings settings)
        {
            bool result;
            IGraphLayout layout = new LayoutFactory().GetLayout(layoutId, settings);
            int index = graphs.FindIndex(graph => graph.Id == graphId);
            Graph g = graphs[index];
            result = layout.SetLayout(ref g);
            if (result) graphs[index] = g;
            return result;
        }

        /// <summary>
        /// Removes loaded graph from memory.
        /// WARNING: unsaved changes in graph will be lost.
        /// </summary>
        /// <param name="id">id of graph to be removed</param>
        public void RemoveGraphFromMemory(int id)
        {
            graphs.Remove(graphs.Find(graph => graph.Id == id));
        }
        #endregion
    }
}
